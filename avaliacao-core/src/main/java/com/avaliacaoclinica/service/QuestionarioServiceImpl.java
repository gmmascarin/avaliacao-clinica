package com.avaliacaoclinica.service;

import com.avaliacaoclinica.converter.AvaliacaoConverter;
import com.avaliacaoclinica.dao.PerguntaDAO;
import com.avaliacaoclinica.dao.QuestionarioDAO;
import com.avaliacaoclinica.dao.UsuarioDAO;
import com.avaliacaoclinica.dao.UsuarioQuestionarioDAO;
import com.avaliacaoclinica.exception.PerguntaException;
import com.avaliacaoclinica.exception.QuestionarioCantBeDeletedException;
import com.avaliacaoclinica.exception.QuestionarioNotFoundException;
import com.avaliacaoclinica.model.PerguntaEntity;
import com.avaliacaoclinica.model.QuestionarioEntity;
import com.avaliacaoclinica.model.RespostaEntity;
import com.avaliacaoclinica.model.TipoPergunta;
import com.avaliacaoclinica.model.UsuarioEntity;
import com.avaliacaoclinica.model.UsuarioQuestionarioEntity;
import com.avaliacaoclinica.vo.ControleAcessoQuestionarioVO;
import com.avaliacaoclinica.vo.NovaPerguntaVO;
import com.avaliacaoclinica.vo.QuestionarioVO;
import com.avaliacaoclinica.vo.SimplesQuestionarioVO;
import com.avaliacaoclinica.vo.UsuarioVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/28/17 1:04 PM
 */
@Service
@Slf4j
public class QuestionarioServiceImpl implements QuestionarioService {

    @Autowired
    private QuestionarioDAO questionarioDAO;

    @Autowired
    private UsuarioQuestionarioDAO usuarioQuestionarioDAO;

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Autowired
    private PerguntaService perguntaService;

    @Autowired
    private PerguntaDAO perguntaDAO;

    @Override
    public List<QuestionarioVO> findAllQuestionariosCompletosByIdUsuario(
            final Long idUsuario) {
        final List<QuestionarioEntity> questionarios = questionarioDAO
                .findByIdUsuario(idUsuario);

        List<QuestionarioVO> questionariosVO = new ArrayList<QuestionarioVO>();
        for (QuestionarioEntity questionarioEntity : questionarios) {
            questionariosVO.add(AvaliacaoConverter.toQuestionarioVO(
                    questionarioEntity));
        }
        return questionariosVO;
    }

    @Override
    public List<SimplesQuestionarioVO> findsAllQuestionarioByIdUsuario(
            final
            Long idUsuario) {
        final List<QuestionarioEntity> questionarios = questionarioDAO
                .findByIdUsuario(idUsuario);
        List<SimplesQuestionarioVO> questionariosVO = new
                ArrayList<SimplesQuestionarioVO>();
        for (QuestionarioEntity questionarioEntity : questionarios) {
            questionariosVO.add(AvaliacaoConverter.toSimplesQuestionarioVO(
                    questionarioEntity));
        }
        return questionariosVO;
    }

    @Override
    public List<SimplesQuestionarioVO> findTop3QuestionariosByIdUsuario(
            final
            Long idUsuario) {
        List<QuestionarioEntity> questionarios = questionarioDAO
                .findTop3ByUsuarioQuestionariosUsuarioEntityOrderByIdDesc(new
                        UsuarioEntity(idUsuario));
        List<SimplesQuestionarioVO> questionariosVO = new
                ArrayList<SimplesQuestionarioVO>();
        questionarios.forEach((q) -> questionariosVO.add(AvaliacaoConverter
                .toQuestionarioVO(q)));
        return questionariosVO;
    }

    @Override
    public QuestionarioVO findLastQuestionarioByIdUsuario(
            final Long idUsuario) {
        UsuarioEntity usuarioEntity = new UsuarioEntity();
        usuarioEntity.setId(idUsuario);
        QuestionarioEntity questionarioEntity = questionarioDAO
                .findTopByUsuarioQuestionariosUsuarioEntity(usuarioEntity);
        return AvaliacaoConverter.toQuestionarioVO(questionarioEntity);
    }

    @Override
    @Transactional
    public QuestionarioVO salvarQuestionario(
            final NovaPerguntaVO
                    novaPerguntaVO)
            throws QuestionarioNotFoundException, PerguntaException {
        QuestionarioEntity questionario;
        validatePergunta(novaPerguntaVO);
        if (novaPerguntaVO.getIdQuestionario() != null) {
            questionario = questionarioDAO.findOne(novaPerguntaVO
                    .getIdQuestionario());
            if (questionario == null) {
                log.error(
                        "Questionario não encontrado. Id={}",
                        novaPerguntaVO.getIdQuestionario());
                throw new QuestionarioNotFoundException();
            }
            questionario = adicionarPerguntaNoQuestionario(
                    questionario,
                    novaPerguntaVO);
        } else {
            questionario = criarNovoQuestionario(novaPerguntaVO);
        }
        return AvaliacaoConverter.toQuestionarioVO(questionario);
    }

    private void validatePergunta(final NovaPerguntaVO novaPerguntaVO) throws
            PerguntaException {
        if (novaPerguntaVO == null) {
            throw new PerguntaException("Pergunta não pode ser nula");
        }
        if (TipoPergunta.EXPRESSAO.equals(TipoPergunta.valueOf(novaPerguntaVO
                .getTipo()))) {
            if (StringUtils.isBlank(novaPerguntaVO.getExpressao())) {
                throw new PerguntaException("Expressão não pode ser nula");
            }
            validateExpressao(novaPerguntaVO.getExpressao(), novaPerguntaVO
                    .getIdQuestionario());
        }
    }

    private void validateExpressao(
            final String expressao, final Integer
            idQuestionario) throws PerguntaException {
        String expressaoTeste = expressao;
        List<Integer> perguntasNaExpressao = perguntaService
                .buscarPerguntasNaExpressao(expressao);

        for (Integer posicaoPergunta : perguntasNaExpressao) {
            PerguntaEntity perguntaEntity = perguntaDAO
                    .findByQuestionarioEntityAndPosicao(
                            new
                                    QuestionarioEntity(idQuestionario),
                            posicaoPergunta);
            if (perguntaEntity == null || !TipoPergunta.MULTIPLA_ESCOLHA
                    .equals(perguntaEntity.getTipo())) {
                throw new PerguntaException(String.format("A pergunta %d não "
                        + "pode ser parte da expressão", posicaoPergunta));
            }
            //Será adicionado peso 1 em todas as perguntas para testar se o
            // cálculo é permitido
            expressaoTeste = perguntaService
                    .adicionarPesoDaPerguntaNaExpressao(expressaoTeste,
                            posicaoPergunta, 1d);
        }
        try {
            perguntaService.calcularExpressaoString(expressaoTeste);
        } catch (ScriptException e) {
            throw new PerguntaException("Expressão incorreta");
        }
    }

    @Override
    public QuestionarioVO findById(final Integer questionarioId) {
        QuestionarioEntity questionarioEntity = questionarioDAO.findOne(
                questionarioId);
        if (questionarioEntity == null) {
            return null;
        }
        return AvaliacaoConverter.toQuestionarioVO(questionarioEntity);
    }

    @Override
    @Transactional
    public Boolean delete(final Integer questionarioId) throws
            QuestionarioCantBeDeletedException {
        final QuestionarioEntity questionarioEntity = questionarioDAO.findOne(
                questionarioId);
        if (questionarioEntity == null) {
            return Boolean.FALSE;
        } else if (questionarioEntity.getAvaliacoes() != null
                && !questionarioEntity.getAvaliacoes().isEmpty()) {
            throw new QuestionarioCantBeDeletedException("Não é possível "
                    + "excluir Questionário que possui avaliações");
        }
        questionarioDAO.delete(questionarioId);
        return Boolean.TRUE;
    }

    @Override
    @Transactional
    public ControleAcessoQuestionarioVO getControleAcesso(
            final Integer questionarioId) {

        List<UsuarioQuestionarioEntity> usuarioQuestionarios =
                usuarioQuestionarioDAO.findByQuestionarioEntity(new
                        QuestionarioEntity(questionarioId));
        List<UsuarioVO> usuariosAutorizados = usuarioQuestionarios.stream()
                .map(usuarioQuestionarioEntity -> AvaliacaoConverter
                        .toUsuarioVO(usuarioDAO.findOne(
                                usuarioQuestionarioEntity.getUsuarioEntity()
                                        .getId()))).collect(Collectors.toList()
                );
        List<UsuarioEntity> allUsers = usuarioDAO.findAll();
        List<UsuarioVO> usuariosNaoAutorizados = allUsers.stream().map(
                AvaliacaoConverter::toUsuarioVO).filter(usuario ->
                !usuariosAutorizados.contains(usuario)).collect(Collectors
                .toList());

        ControleAcessoQuestionarioVO controleAcessoQuestionarioVO = new
                ControleAcessoQuestionarioVO();
        controleAcessoQuestionarioVO.setQuestionarioId(questionarioId);
        controleAcessoQuestionarioVO.setUsuariosAutorizados(
                usuariosAutorizados);
        controleAcessoQuestionarioVO.setUsuariosNaoAutorizados(
                usuariosNaoAutorizados);

        return controleAcessoQuestionarioVO;
    }

    @Override
    @Transactional
    public ControleAcessoQuestionarioVO updateControleAcesso(
            final ControleAcessoQuestionarioVO controleAcessoQuestionarioVO) {
        List<UsuarioQuestionarioEntity> usuarioQuestionarioAtual =
                usuarioQuestionarioDAO.findByQuestionarioEntity(new
                        QuestionarioEntity(controleAcessoQuestionarioVO
                        .getQuestionarioId()));
        usuarioQuestionarioAtual.forEach(usuarioQuestionarioEntity ->
                usuarioQuestionarioDAO.delete(usuarioQuestionarioEntity.getId()
                ));

        controleAcessoQuestionarioVO.getUsuariosAutorizados().forEach(
                usuarioAutorizadoVO ->
                        usuarioQuestionarioDAO.save(new
                                UsuarioQuestionarioEntity(
                                null, AvaliacaoConverter.toUsuarioEntity(
                                usuarioAutorizadoVO), new
                                QuestionarioEntity(
                                controleAcessoQuestionarioVO
                                        .getQuestionarioId())))
        );
        return getControleAcesso(controleAcessoQuestionarioVO
                .getQuestionarioId());
    }

    private QuestionarioEntity adicionarPerguntaNoQuestionario(
                    final QuestionarioEntity questionarioEntity,
                    final NovaPerguntaVO novaPerguntaVO) {
        Integer proximaPosicaoPergunta = getProximaPosicaoPergunta(
                questionarioEntity.getPerguntas());
        PerguntaEntity perguntaEntity = criarPergunta(
                novaPerguntaVO,
                proximaPosicaoPergunta);
        perguntaEntity.setQuestionarioEntity(questionarioEntity);

        questionarioEntity.getPerguntas().add(perguntaEntity);

        return questionarioDAO.save(questionarioEntity);

    }

    private Integer getProximaPosicaoPergunta(
            final List<PerguntaEntity> perguntas) {
        Integer posicaoMaxima = 0;
        for (PerguntaEntity pergunta : perguntas) {
            if (pergunta.getPosicao() > posicaoMaxima) {
                posicaoMaxima = pergunta.getPosicao();
            }
        }
        return posicaoMaxima + 1;
    }

    private QuestionarioEntity criarNovoQuestionario(
            final NovaPerguntaVO
                    novaPerguntaVO) {
        QuestionarioEntity questionario = new QuestionarioEntity();
        UsuarioEntity usuario = usuarioDAO.findOne(novaPerguntaVO
                .getIdUsuario());
        questionario.setTitulo(novaPerguntaVO.getTituloQuestionario());

        questionario.setUsuarioQuestionarios(Collections.singletonList(new
                UsuarioQuestionarioEntity(null, usuario, questionario)));

        PerguntaEntity perguntaEntity = criarPergunta(novaPerguntaVO, 1);
        perguntaEntity.setQuestionarioEntity(questionario);

        questionario.setPerguntas(Collections.singletonList(perguntaEntity));

        return questionarioDAO.save(questionario);
    }

    private PerguntaEntity criarPergunta(
            final NovaPerguntaVO novaPerguntaVO,
            final Integer posicao) {
        PerguntaEntity perguntaEntity = new PerguntaEntity();
        perguntaEntity.setPosicao(posicao);
        perguntaEntity.setTexto(novaPerguntaVO.getTexto());
        perguntaEntity.setExpressao(novaPerguntaVO.getExpressao());
        TipoPergunta tipoPergunta = TipoPergunta.valueOf(novaPerguntaVO
                .getTipo());
        perguntaEntity.setTipo(tipoPergunta);
        if (tipoPergunta.equals(TipoPergunta.MULTIPLA_ESCOLHA)) {
            List<RespostaEntity> respostas = new ArrayList<RespostaEntity>();
            for (int i = 0; i < novaPerguntaVO.getOpcao().size(); i++) {
                String opcao = novaPerguntaVO.getOpcao().get(i);
                if (StringUtils.isNotBlank(opcao)) {
                    respostas.add(createRespostaEntity(perguntaEntity, opcao,
                            novaPerguntaVO.getPeso().get(i)));
                }
            }
            perguntaEntity.setRespostas(respostas);
        }
        return perguntaEntity;
    }

    private RespostaEntity createRespostaEntity(
            final PerguntaEntity perguntaEntity,
            final String texto, final Double peso) {
        RespostaEntity respostaEntity = new RespostaEntity();
        respostaEntity.setPosicao(1);
        respostaEntity.setTexto(texto);
        respostaEntity.setPeso(peso);
        respostaEntity.setPerguntaEntity(perguntaEntity);
        return respostaEntity;
    }
}
