package com.avaliacaoclinica.service;

import com.avaliacaoclinica.vo.HomeAppVO;
import com.avaliacaoclinica.vo.HomeVO;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 10/19/17 6:13 PM
 */
public interface HomeService {

    HomeVO home(Long usuarioId);

    HomeAppVO homeApp(Long usuarioId);

}
