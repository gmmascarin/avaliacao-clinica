package com.avaliacaoclinica.service;

import com.avaliacaoclinica.converter.AvaliacaoConverter;
import com.avaliacaoclinica.dao.AvaliacaoDAO;
import com.avaliacaoclinica.dao.AvaliadoDAO;
import com.avaliacaoclinica.dao.PerguntaDAO;
import com.avaliacaoclinica.dao.PerguntaRespostaDAO;
import com.avaliacaoclinica.dao.QuestionarioDAO;
import com.avaliacaoclinica.dao.RespostaDAO;
import com.avaliacaoclinica.exception.AvaliadoNotFoundException;
import com.avaliacaoclinica.exception.PerguntaNotFoundException;
import com.avaliacaoclinica.exception.QuestionarioNotFoundException;
import com.avaliacaoclinica.exception.RespostaNotFoundException;
import com.avaliacaoclinica.model.AvaliacaoEntity;
import com.avaliacaoclinica.model.AvaliadoEntity;
import com.avaliacaoclinica.model.GrupoAtendimentoEnum;
import com.avaliacaoclinica.model.PerguntaEntity;
import com.avaliacaoclinica.model.PerguntaRespostaEntity;
import com.avaliacaoclinica.model.QuestionarioEntity;
import com.avaliacaoclinica.model.RespostaEntity;
import com.avaliacaoclinica.model.SexoEnum;
import com.avaliacaoclinica.model.TipoPergunta;
import com.avaliacaoclinica.model.UsuarioEntity;
import com.avaliacaoclinica.util.DateUtil;
import com.avaliacaoclinica.vo.AvaliacaoVO;
import com.avaliacaoclinica.vo.AvaliadoVO;
import com.avaliacaoclinica.vo.EfetivaAvaliacaoVO;
import com.avaliacaoclinica.vo.ExportacaoVO;
import com.avaliacaoclinica.vo.GraficoItemVO;
import com.avaliacaoclinica.vo.GraficoVO;
import com.avaliacaoclinica.vo.PerguntaRespostaVO;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.script.ScriptException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 6:10 PM
 */
@Service
@Log4j
public class AvaliacaoServiceImpl implements AvaliacaoService {

    @Autowired
    private PerguntaDAO perguntaDAO;

    @Autowired
    private RespostaDAO respostaDAO;

    @Autowired
    private AvaliacaoDAO avaliacaoDAO;

    @Autowired
    private AvaliadoDAO avaliadoDAO;

    @Autowired
    private QuestionarioDAO questionarioDAO;

    @Autowired
    private PerguntaRespostaDAO perguntaRespostaDAO;

    @Autowired
    private PerguntaService perguntaService;

    private static final int REPORT_MAX_BAR = 5;
    private static final int PERCENT_NUMBER = 100;

    @Override
    @Transactional
    public Boolean avaliar(final EfetivaAvaliacaoVO efetivaAvaliacaoVO)
            throws PerguntaNotFoundException, RespostaNotFoundException,
            AvaliadoNotFoundException,
            QuestionarioNotFoundException, ScriptException {
        log.info("M=avaliar, msg=inicando, efetivaAvaliacaoVO="
                + efetivaAvaliacaoVO);

        AvaliadoEntity avaliado = avaliadoDAO.findByCpf(efetivaAvaliacaoVO
                .getAvaliadoVO().getCpf());
        if (avaliado == null) {
            log.info("M=avaliar, msg=novo avaliado");
            avaliado = new AvaliadoEntity();
        }
        preencherAvaliadoEntity(avaliado, efetivaAvaliacaoVO.getAvaliadoVO());
        avaliado = avaliadoDAO.save(avaliado);

        final QuestionarioEntity questionarioEntity = questionarioDAO.findOne(
                efetivaAvaliacaoVO.getIdQuestionario());
        if (questionarioEntity == null) {
            log.info("M=avaliar, msg=novo questionario");
            throw new QuestionarioNotFoundException();
        }

        AvaliacaoEntity avaliacaoEntity = avaliacaoDAO
                .findByAvaliadoEntityAndQuestionarioEntity(avaliado,
                        questionarioEntity);
        if (avaliacaoEntity == null || avaliacaoEntity.getId() == null) {
            log.info("M=avaliar, msg=nova avaliacao");
            avaliacaoEntity = new AvaliacaoEntity();
        } else {
            log.info("M=avaliar, msg=excluindo as respostas anteriores");
            for (PerguntaRespostaEntity perguntaRespostaEntity
                    : avaliacaoEntity.getListPerguntaRespostaEntity()) {
                perguntaRespostaDAO.delete(perguntaRespostaEntity.getId());
            }
        }
        avaliacaoEntity.setData(Calendar.getInstance());
        avaliacaoEntity.setAvaliadoEntity(avaliado);
        avaliacaoEntity.setQuestionarioEntity(questionarioEntity);
        avaliacaoEntity.setListPerguntaRespostaEntity(getPerguntasRespostas(
                efetivaAvaliacaoVO, avaliacaoEntity));

        avaliacaoEntity = avaliacaoDAO.save(avaliacaoEntity);
        if (avaliacaoEntity != null) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    private void preencherAvaliadoEntity(final AvaliadoEntity avaliadoEntity,
                                         final AvaliadoVO avaliadoVO) {
        avaliadoEntity.setNomeAvaliado(avaliadoVO.getNome());
        avaliadoEntity.setCpf(avaliadoVO.getCpf());
        avaliadoEntity.setIdentificadorAtendimento(avaliadoVO
                .getIdentificadorAtendimento());
        avaliadoEntity.setSexo(SexoEnum.parse(avaliadoVO.getSexoAbreviado()));
        avaliadoEntity.setNascimento(DateUtil.getDateAsCalendar(avaliadoVO
                .getNascimento()));
        avaliadoEntity.setGrupoAtendimento(GrupoAtendimentoEnum.parse(
                avaliadoVO.getGrupoAtendimentoId()));
    }

    private List<PerguntaRespostaEntity> getPerguntasRespostas(
            final EfetivaAvaliacaoVO efetivaAvaliacaoVO,
            final AvaliacaoEntity avaliacaoEntity)
            throws PerguntaNotFoundException, RespostaNotFoundException,
            ScriptException {
        List<PerguntaRespostaEntity> listaPerguntaResposta = new ArrayList<>();
        for (PerguntaRespostaVO perguntaRespostaVO : efetivaAvaliacaoVO
                .getPerguntasRespostas()) {
            PerguntaRespostaEntity perguntaRespostaEntity = new
                    PerguntaRespostaEntity();

            final PerguntaEntity perguntaEntity = perguntaDAO.findOne(
                    perguntaRespostaVO.getIdPergunta());
            if (perguntaEntity == null) {
                throw new PerguntaNotFoundException();
            }
            perguntaRespostaEntity.setPerguntaEntity(perguntaEntity);

            if (!perguntaEntity.getTipo().equals(TipoPergunta.EXPRESSAO)) {
                //A perguntas de expressao são calculadas no final
                perguntaRespostaEntity.setTexto(perguntaRespostaVO
                        .getTextoResposta());
            } else {
                perguntaRespostaEntity.setTexto(calcularExpressao(
                        perguntaEntity.getExpressao(), listaPerguntaResposta));
            }

            if (perguntaRespostaVO.getIdResposta() != null
                    && perguntaRespostaVO.getIdResposta() > 0) {
                final RespostaEntity respostaEntity = respostaDAO.findOne(
                        perguntaRespostaVO.getIdResposta());
                if (respostaEntity == null) {
                    throw new RespostaNotFoundException();
                }
                perguntaRespostaEntity.setRespostaEntity(respostaEntity);
            }

            perguntaRespostaEntity.setAvaliacaoEntity(avaliacaoEntity);
            listaPerguntaResposta.add(perguntaRespostaEntity);
        }
        return listaPerguntaResposta;
    }

    private String calcularExpressao(final String expressao, final
    List<PerguntaRespostaEntity> listaPerguntaResposta)
            throws ScriptException, PerguntaNotFoundException {

        if (expressao == null || listaPerguntaResposta == null
                || listaPerguntaResposta.isEmpty()) {
            throw new IllegalArgumentException("Não foi possível calcular a "
                    + "expressão");
        }

        log.info("M=calcularExpressao, expressao=" + expressao);

        String expressaoComValorDoP = expressao;

        List<Integer> posicaoPerguntas = perguntaService
                .buscarPerguntasNaExpressao(expressaoComValorDoP);
        for (final Integer posicaoPergunta : posicaoPerguntas) {
            boolean encontrou = Boolean.FALSE;

            for (final PerguntaRespostaEntity perguntaRespostaEntity
                    : listaPerguntaResposta) {
                if (perguntaRespostaEntity.getPerguntaEntity().getPosicao()
                        .equals(posicaoPergunta)) {
                    final Double peso = perguntaRespostaEntity
                            .getRespostaEntity().getPeso();
                    if (peso != null && peso > 0) {
                        expressaoComValorDoP = perguntaService
                                .adicionarPesoDaPerguntaNaExpressao(
                                        expressaoComValorDoP,
                                        posicaoPergunta, peso);
                        encontrou = Boolean.TRUE;
                    }
                    break;
                }
            }
            if (!encontrou) {
                throw new PerguntaNotFoundException();
            }
        }
        return String.valueOf(perguntaService.calcularExpressaoString(
                expressaoComValorDoP.replaceAll("P|p", "")));
    }

    @Override
    public List<AvaliacaoVO> buscarTodasPeloIdUsuario(final Long idUsuario) {
        final List<AvaliacaoEntity> avaliacoes = avaliacaoDAO
                .buscarTodasPeloIdUsuario(idUsuario);
        List<AvaliacaoVO> avaliacoesVO = new ArrayList<>();
        for (AvaliacaoEntity avaliacao : avaliacoes) {
            avaliacoesVO.add(AvaliacaoConverter.toAvaliacaoVO(avaliacao));
        }
        return avaliacoesVO;
    }

    @Override
    @SuppressWarnings(value = "checkstyle:linelength")
    public List<AvaliacaoVO> findTop10ByUserId(final Long idUsuario) {
        List<AvaliacaoEntity> avaliacoes = avaliacaoDAO.findTop10ByQuestionarioEntityUsuarioQuestionariosUsuarioEntityOrderByIdDesc(new UsuarioEntity(idUsuario));
        List<AvaliacaoVO> avaliacoesVO = new ArrayList<>();
        avaliacoes.forEach((a) -> avaliacoesVO.add(AvaliacaoConverter
                .toAvaliacaoVO(a)));
        return avaliacoesVO;
    }

    @Override
    @Transactional
    public Boolean delete(final Long avaliacaoId) {
        avaliacaoDAO.delete(avaliacaoId);
        return Boolean.TRUE;
    }

    @Override
    public List<ExportacaoVO> export(final Integer questionarioId,
                                     final Long avaliadoId) {
        return avaliacaoDAO.exportar(questionarioId, avaliadoId);
    }

    @Override
    public List<GraficoVO> getGraficosInfo(final Integer questionarioId,
                                           final Long perguntaParamId) {
        final List<Object[]> qtdRespostaDasPerguntasDoQuestionario =
                avaliacaoDAO
                        .findQtdRespostaDasPerguntasDoQuestionario(
                                questionarioId, perguntaParamId);
        Long lastPerguntaId = 0L;
        List<GraficoVO> graficoVOS = new ArrayList<>();
        GraficoVO graficoVO = new GraficoVO();
        graficoVOS.add(graficoVO);
        List<GraficoItemVO> itens = new ArrayList<>();
        graficoVO.setItem(itens);

        for (final Object[] object : qtdRespostaDasPerguntasDoQuestionario) {
            Long perguntaId = ((Integer) object[0]).longValue();
            String resposta = (String) object[1];
            Integer quantidade = ((BigInteger) object[2]).intValue();

            if (lastPerguntaId == 0L) {
                final PerguntaEntity perguntaEntity = perguntaDAO.findOne(
                        perguntaId);
                graficoVO.setPergunta(perguntaEntity.getTexto());
            } else if (!lastPerguntaId.equals(perguntaId)) {
                //Se for diferente da pergunta anterior
                graficoVO.setItem(itens);

                graficoVO = new GraficoVO();
                graficoVOS.add(graficoVO);

                itens = new ArrayList<>();
                graficoVO.setItem(itens);
                final PerguntaEntity perguntaEntity = perguntaDAO.findOne(
                        perguntaId);
                graficoVO.setPergunta(perguntaEntity.getTexto());
            }

            itens.add(new GraficoItemVO(resposta, quantidade));

            lastPerguntaId = perguntaId;
        }

        for (final GraficoVO vo : graficoVOS) {

            //Descobriremos o total de respostas
            int totalValue = 0;
            for (final GraficoItemVO itemVO : vo.getItem()) {
                totalValue += itemVO.getValue();
            }

            //Se a quantidade de itens for maior que 5, iremos adicionar
            // "outros"
            if (vo.getItem() != null && !vo.getItem().isEmpty() && vo.getItem()
                    .size() > REPORT_MAX_BAR) {

                int totalOutros = 0;
                for (int i = REPORT_MAX_BAR - 1; i < vo.getItem().size(); i++) {
                    totalOutros += vo.getItem().get(i).getValue();
                    vo.getItem().remove(i);
                }
                vo.getItem().add(new GraficoItemVO("Outros", totalOutros));
            }

            //Calcularemos a porcentagem
            for (final GraficoItemVO itemVO : vo.getItem()) {
                itemVO.setValue((itemVO.getValue() * PERCENT_NUMBER)
                        / totalValue);
            }
        }
        return graficoVOS;
    }

    @Override
    public List<AvaliacaoVO> buscarTodasPeloIdQuestionario(
            final Integer questionarioId) {
        final List<AvaliacaoEntity> avaliacoes = avaliacaoDAO
                .buscarTodasPeloIdQuestionario(questionarioId);
        List<AvaliacaoVO> avaliacoesVO = new ArrayList<>();
        for (AvaliacaoEntity avaliacao : avaliacoes) {
            avaliacoesVO.add(AvaliacaoConverter.toAvaliacaoVO(avaliacao));
        }
        return avaliacoesVO;
    }
}
