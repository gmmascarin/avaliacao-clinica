package com.avaliacaoclinica.service;

import com.avaliacaoclinica.converter.AvaliacaoConverter;
import com.avaliacaoclinica.dao.AvaliadoDAO;
import com.avaliacaoclinica.exception.AvaliadoJaCadastradoException;
import com.avaliacaoclinica.model.AvaliadoEntity;
import com.avaliacaoclinica.vo.AvaliadoVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 9:16 PM
 */
@Service
@Slf4j
public class AvaliadoServiceImpl implements AvaliadoService {

    @Autowired
    private AvaliadoDAO avaliadoDAO;

    @Override
    @Transactional
    public Boolean salvarAvaliado(final AvaliadoVO avaliadoVO) throws
            AvaliadoJaCadastradoException {
        log.info("M=salvarAvaliado, avaliadoVO=" + avaliadoVO);

        AvaliadoEntity avaliadoEntity = avaliadoDAO.findByCpf(avaliadoVO
                .getCpf());
        if (avaliadoEntity != null) {
            throw new AvaliadoJaCadastradoException();
        }
        avaliadoEntity = AvaliacaoConverter.toAvaliadoEntity(avaliadoVO);
        avaliadoEntity = avaliadoDAO.save(avaliadoEntity);
        if (avaliadoEntity != null) {
            return Boolean.TRUE;
        }
        log.error("M=salvarAvaliado, nao foi possivel inserir no bd");
        return Boolean.FALSE;
    }

    @Override
    public List<AvaliadoVO> buscarAvaliadosPorQuestionarioId(
            final Integer
                    questionarioId) {
        final List<AvaliadoEntity> avaliados = avaliadoDAO
                .buscarAvaliadosPorQuestionarioId(questionarioId);
        List<AvaliadoVO> avaliadosVO = new ArrayList<AvaliadoVO>();
        for (AvaliadoEntity avaliado : avaliados) {
            avaliadosVO.add(AvaliacaoConverter.toAvaliadoVO(avaliado));
        }
        return avaliadosVO;
    }
}
