package com.avaliacaoclinica.service;

import com.avaliacaoclinica.dao.AvaliacaoDAO;
import com.avaliacaoclinica.dao.PerguntaDAO;
import com.avaliacaoclinica.dao.QuestionarioDAO;
import com.avaliacaoclinica.dao.UsuarioDAO;
import com.avaliacaoclinica.vo.AvaliacaoVO;
import com.avaliacaoclinica.vo.HomeAppVO;
import com.avaliacaoclinica.vo.HomeVO;
import com.avaliacaoclinica.vo.QuestionarioVO;
import com.avaliacaoclinica.vo.SimplesQuestionarioVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/19/17 6:14 PM
 */
@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    private QuestionarioDAO questionarioDAO;

    @Autowired
    private AvaliacaoDAO avaliacaoDAO;

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Autowired
    private PerguntaDAO perguntaDAO;

    @Autowired
    private QuestionarioService questionarioService;

    @Autowired
    private AvaliacaoService avaliacaoService;

    @Override
    public HomeVO home(final Long usuarioId) {
        final Long countQuestionarios = questionarioDAO.count();
        final Long countAvaliacoes = avaliacaoDAO.count();
        final Long countUsuarios = usuarioDAO.count();
        final Long countPerguntas = perguntaDAO.count();

        final List<SimplesQuestionarioVO> questionarios = questionarioService
                .findTop3QuestionariosByIdUsuario(usuarioId);

        final List<AvaliacaoVO> avaliacoes = avaliacaoService
                .findTop10ByUserId(usuarioId);

        return new HomeVO(countQuestionarios.intValue(), countAvaliacoes
                .intValue(),
                countUsuarios.intValue(),
                countPerguntas.intValue(), questionarios, avaliacoes);
    }

    @Override
    public HomeAppVO homeApp(final Long usuarioId) {
        final QuestionarioVO questionario = questionarioService
                .findLastQuestionarioByIdUsuario(usuarioId);
        if (questionario != null) {
            HomeAppVO homeAppVO = new HomeAppVO();
            homeAppVO.setTituloUltimoQuestionario(questionario.getTitulo());
            homeAppVO.setTotalAvaliacoesUltimoQuestionario(questionario
                    .getCountAvaliacoes().longValue());
            if (questionario.getPerguntas() != null && !questionario
                    .getPerguntas().isEmpty()) {
                homeAppVO.setTotalPerguntasUltimoQuestionario((long)
                        questionario.getPerguntas().size());
            } else {
                homeAppVO.setTotalPerguntasUltimoQuestionario(0L);
            }
            return homeAppVO;
        }
        return new HomeAppVO();
    }
}
