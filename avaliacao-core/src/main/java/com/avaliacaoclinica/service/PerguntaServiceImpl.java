package com.avaliacaoclinica.service;

import com.avaliacaoclinica.converter.AvaliacaoConverter;
import com.avaliacaoclinica.dao.PerguntaDAO;
import com.avaliacaoclinica.exception.PerguntaCantBeDeletedException;
import com.avaliacaoclinica.model.PerguntaEntity;
import com.avaliacaoclinica.vo.PerguntaVO;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 11:38 AM
 */
@Service
@Log4j
public class PerguntaServiceImpl implements PerguntaService {

    @Autowired
    private PerguntaDAO perguntaDAO;

    @Override
    public List<PerguntaVO> findAllByQuestionarioId(final Integer
                                                            questionarioId) {
        List<PerguntaVO> perguntaVOS = new ArrayList<PerguntaVO>();
        for (PerguntaEntity pergunta : perguntaDAO.findAllByQuestionarioId(
                questionarioId)) {
            perguntaVOS.add(AvaliacaoConverter.toPerguntaVO(pergunta));
        }
        return perguntaVOS;
    }

    @Override
    public PerguntaVO findOne(final Long perguntaId) {
        return AvaliacaoConverter.toPerguntaVO(perguntaDAO.findOne(perguntaId));
    }

    @Override
    public Boolean delete(final Long perguntaId) throws
            PerguntaCantBeDeletedException {
        PerguntaEntity pergunta = perguntaDAO.findOne(perguntaId);
        if (pergunta == null) {
            throw new IllegalArgumentException("Pergunta não encontrada");
        }
        if (pergunta.getQuestionarioEntity().getAvaliacoes() != null
                && !pergunta.getQuestionarioEntity().getAvaliacoes().isEmpty()
                ) {
            throw new PerguntaCantBeDeletedException("Não é possível excluir "
                    + "pergunta de questionário que possui avaliações");
        }

        perguntaDAO.delete(perguntaId);
        return Boolean.TRUE;
    }

    @Override
    public List<Integer> buscarPerguntasNaExpressao(final String expressao) {
        List<Integer> perguntas = new ArrayList<>();
        for (int i = 0; i < expressao.length() - 1; i++) {
            char caracter = expressao.charAt(i);
            if (caracter == 'P' || caracter == 'p') {
                int j = i;
                StringBuilder pergunta = new StringBuilder();
                do {
                    j++;
                    pergunta.append(expressao.charAt(j));
                }
                while ((j + 1) < expressao.length() && StringUtils.isNumeric(
                        String.valueOf(expressao.charAt(j + 1))));
                perguntas.add(Integer.parseInt(pergunta.toString()));
            }
        }
        return perguntas;
    }

    @Override
    public String adicionarPesoDaPerguntaNaExpressao(final String expressao,
                                                     final Integer posicao,
                                                     final Double peso) {
        String expressaoSubstituida = expressao;
        expressaoSubstituida = expressaoSubstituida.replaceAll("p" + posicao,
                peso.toString());
        expressaoSubstituida = expressaoSubstituida.replaceAll("P" + posicao,
                peso.toString());
        return expressaoSubstituida;
    }

    @Override
    public Double calcularExpressaoString(final String expressao) throws
            ScriptException {
        log.info("M=calcularExpressaoString, expressao=" + expressao);
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        return (Double) engine.eval(expressao);
    }
}
