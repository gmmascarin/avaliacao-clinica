package com.avaliacaoclinica.service;

import com.avaliacaoclinica.exception.PerguntaException;
import com.avaliacaoclinica.exception.QuestionarioCantBeDeletedException;
import com.avaliacaoclinica.exception.QuestionarioNotFoundException;
import com.avaliacaoclinica.vo.ControleAcessoQuestionarioVO;
import com.avaliacaoclinica.vo.NovaPerguntaVO;
import com.avaliacaoclinica.vo.QuestionarioVO;
import com.avaliacaoclinica.vo.SimplesQuestionarioVO;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/28/17 1:04 PM
 */
public interface QuestionarioService {

    List<QuestionarioVO> findAllQuestionariosCompletosByIdUsuario(
            Long idUsuario);

    List<SimplesQuestionarioVO> findsAllQuestionarioByIdUsuario(Long idUsuario);

    List<SimplesQuestionarioVO> findTop3QuestionariosByIdUsuario(
            Long idUsuario);

    QuestionarioVO findLastQuestionarioByIdUsuario(Long idUsuario);

    QuestionarioVO salvarQuestionario(NovaPerguntaVO novaPerguntaVO) throws
            QuestionarioNotFoundException, PerguntaException;

    QuestionarioVO findById(Integer questionarioId);

    Boolean delete(Integer questionarioId) throws
            QuestionarioCantBeDeletedException;

    ControleAcessoQuestionarioVO getControleAcesso(Integer questionarioId);

    ControleAcessoQuestionarioVO updateControleAcesso(
            ControleAcessoQuestionarioVO controleAcessoQuestionarioVO);
}
