package com.avaliacaoclinica.service;

import com.avaliacaoclinica.exception.AvaliadoNotFoundException;
import com.avaliacaoclinica.exception.PerguntaNotFoundException;
import com.avaliacaoclinica.exception.QuestionarioNotFoundException;
import com.avaliacaoclinica.exception.RespostaNotFoundException;
import com.avaliacaoclinica.vo.AvaliacaoVO;
import com.avaliacaoclinica.vo.EfetivaAvaliacaoVO;
import com.avaliacaoclinica.vo.ExportacaoVO;
import com.avaliacaoclinica.vo.GraficoVO;

import java.util.List;

import javax.script.ScriptException;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 6:10 PM
 */
public interface AvaliacaoService {

    Boolean avaliar(EfetivaAvaliacaoVO avaliacaoVO)
            throws PerguntaNotFoundException, RespostaNotFoundException,
            AvaliadoNotFoundException,
            QuestionarioNotFoundException, ScriptException;

    List<AvaliacaoVO> buscarTodasPeloIdUsuario(Long idUsuario);

    List<AvaliacaoVO> findTop10ByUserId(Long idUsuario);

    Boolean delete(Long avaliacaoId);

    List<ExportacaoVO> export(Integer questionarioId, Long avaliadoId);

    List<GraficoVO> getGraficosInfo(Integer questionarioId, Long perguntaId);

    List<AvaliacaoVO> buscarTodasPeloIdQuestionario(Integer questionarioId);
}
