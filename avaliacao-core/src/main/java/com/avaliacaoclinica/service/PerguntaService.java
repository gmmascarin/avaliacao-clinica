package com.avaliacaoclinica.service;

import com.avaliacaoclinica.exception.PerguntaCantBeDeletedException;
import com.avaliacaoclinica.vo.PerguntaVO;

import javax.script.ScriptException;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 11:38 AM
 */
public interface PerguntaService {
    List<PerguntaVO> findAllByQuestionarioId(Integer questionarioId);

    PerguntaVO findOne(Long perguntaId);

    Boolean delete(Long perguntaId) throws PerguntaCantBeDeletedException;

    List<Integer> buscarPerguntasNaExpressao(String expressao);

    String adicionarPesoDaPerguntaNaExpressao(String expressao, Integer
            posicao, Double peso);

    Double calcularExpressaoString(String expressao) throws ScriptException;
}
