package com.avaliacaoclinica.service;

import java.util.List;

import com.avaliacaoclinica.exception.AvaliadoJaCadastradoException;
import com.avaliacaoclinica.vo.AvaliadoVO;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 9:16 PM
 */
public interface AvaliadoService {

    Boolean salvarAvaliado(AvaliadoVO avaliadoVO) throws
            AvaliadoJaCadastradoException;

    List<AvaliadoVO> buscarAvaliadosPorQuestionarioId(Integer questionarioId);

}
