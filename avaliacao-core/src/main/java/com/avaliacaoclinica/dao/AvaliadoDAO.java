package com.avaliacaoclinica.dao;

import com.avaliacaoclinica.model.AvaliadoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 8:45 PM
 */
public interface AvaliadoDAO extends JpaRepository<AvaliadoEntity, Long> {

    AvaliadoEntity findByCpf(String cpf);

    @Query("select a from AvaliadoEntity a JOIN a.avaliacoes av where av"
            + ".questionarioEntity.id=:questionarioId")
    List<AvaliadoEntity> buscarAvaliadosPorQuestionarioId(
            @Param("questionarioId") Integer questionarioId);

}
