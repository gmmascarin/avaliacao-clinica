package com.avaliacaoclinica.dao;

import com.avaliacaoclinica.model.PerguntaRespostaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 8:54 PM
 */
public interface PerguntaRespostaDAO extends
        JpaRepository<PerguntaRespostaEntity, Long> {

}
