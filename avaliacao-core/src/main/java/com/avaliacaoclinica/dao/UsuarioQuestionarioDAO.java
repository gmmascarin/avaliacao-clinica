package com.avaliacaoclinica.dao;

import com.avaliacaoclinica.model.QuestionarioEntity;
import com.avaliacaoclinica.model.UsuarioQuestionarioEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioQuestionarioDAO extends
        CrudRepository<UsuarioQuestionarioEntity, Long> {

    List<UsuarioQuestionarioEntity> findByQuestionarioEntity(
            QuestionarioEntity questionarioEntity);
}
