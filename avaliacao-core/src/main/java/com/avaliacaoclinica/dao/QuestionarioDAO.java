package com.avaliacaoclinica.dao;

import com.avaliacaoclinica.model.QuestionarioEntity;
import com.avaliacaoclinica.model.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/28/17 1:23 PM
 */
@Repository
public interface QuestionarioDAO extends JpaRepository<QuestionarioEntity,
        Integer> {

    @Query("SELECT q FROM QuestionarioEntity q"
            + " JOIN q.usuarioQuestionarios uq where uq.usuarioEntity.id = "
            + ":idUsuario ORDER BY q.id DESC")
    List<QuestionarioEntity> findByIdUsuario(@Param("idUsuario") Long
                                                     idUsuario);

    QuestionarioEntity findTopByUsuarioQuestionariosUsuarioEntity(
            UsuarioEntity usuarioEntity);

    List<QuestionarioEntity>
    findTop3ByUsuarioQuestionariosUsuarioEntityOrderByIdDesc(
            UsuarioEntity usuarioEntity);

}
