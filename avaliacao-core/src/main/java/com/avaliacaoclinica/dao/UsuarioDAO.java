package com.avaliacaoclinica.dao;

import com.avaliacaoclinica.model.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by gustavo on 24/12/16.
 */
@Repository
public interface UsuarioDAO extends JpaRepository<UsuarioEntity, Long> {

    UsuarioEntity findByLogin(String login);

}
