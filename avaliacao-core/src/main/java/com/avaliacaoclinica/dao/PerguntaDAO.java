package com.avaliacaoclinica.dao;

import com.avaliacaoclinica.model.PerguntaEntity;
import com.avaliacaoclinica.model.QuestionarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 11:46 AM
 */
@Repository
public interface PerguntaDAO extends JpaRepository<PerguntaEntity, Long> {

    @Query("select p from PerguntaEntity p where p.questionarioEntity.id = "
            + ":questionarioId")
    List<PerguntaEntity> findAllByQuestionarioId(@Param("questionarioId")
                                                         Integer
                                                         questionarioId);

    PerguntaEntity findByQuestionarioEntityAndPosicao(
            QuestionarioEntity questionarioEntity, Integer posicao);

}
