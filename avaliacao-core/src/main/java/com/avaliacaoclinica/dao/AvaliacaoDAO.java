package com.avaliacaoclinica.dao;

import com.avaliacaoclinica.model.AvaliacaoEntity;
import com.avaliacaoclinica.model.AvaliadoEntity;
import com.avaliacaoclinica.model.QuestionarioEntity;
import com.avaliacaoclinica.model.UsuarioEntity;
import com.avaliacaoclinica.vo.ExportacaoVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 6:36 PM
 */
@Repository
public interface AvaliacaoDAO extends JpaRepository<AvaliacaoEntity, Long> {

    @Query("select a from AvaliacaoEntity a"
            + " JOIN a.questionarioEntity q"
            + " JOIN q.usuarioQuestionarios uq"
            + " JOIN uq.usuarioEntity u where u.id = :IdUsuario")
    List<AvaliacaoEntity> buscarTodasPeloIdUsuario(@Param("IdUsuario") Long
                                                           idUsuario);

    List<AvaliacaoEntity>
    findTop10ByQuestionarioEntityUsuarioQuestionariosUsuarioEntityOrderByIdDesc(
            UsuarioEntity usuarioEntity);

    @Query("select a from AvaliacaoEntity a where a.questionarioEntity.id = "
            + ":IdQuestionario")
    List<AvaliacaoEntity> buscarTodasPeloIdQuestionario(
            @Param("IdQuestionario") Integer idQuestionario);

    @Query("SELECT NEW com.avaliacaoclinica.vo.ExportacaoVO(av.nomeAvaliado, "
            + "p.texto, CASE WHEN (pr.texto IS null) THEN r.texto ELSE pr.texto"
            + " END)"
            + " FROM QuestionarioEntity q JOIN q.perguntas p"
            + " JOIN p.perguntasRespostas pr JOIN pr.avaliacaoEntity a"
            + " JOIN a.avaliadoEntity av LEFT OUTER JOIN pr.respostaEntity r"
            + " WHERE q.id = :questionarioId AND (av.id = :avaliadoId OR "
            + ":avaliadoId = 0) "
            + " ORDER BY av.id, p.posicao")
    List<ExportacaoVO> exportar(@Param("questionarioId") Integer questionarioId,
                                @Param("avaliadoId") Long avaliadoId);

    AvaliacaoEntity findByAvaliadoEntityAndQuestionarioEntity(
            AvaliadoEntity avaliadoEntity,
            QuestionarioEntity questionarioEntity);

    @Query(value = "SELECT p.id_pergunta as pergunta,"
            + "IF(pr.texto_resposta is null, r.texto, pr.texto_resposta) as "
            + "resposta, "
            + "COUNT(IF(pr.texto_resposta is null, r.texto, pr.texto_resposta))"
            + " as quantidade "
            + "FROM avaliacao a "
            + "INNER JOIN pergunta_resposta pr on a.id_avaliacao = pr"
            + ".id_avaliacao "
            + "INNER JOIN pergunta p on p.id_pergunta = pr.id_pergunta "
            + "LEFT JOIN resposta r on r.id_resposta = pr.id_resposta "
            + "WHERE a.id_questionario = :questionarioId "
            + " AND (p.id_pergunta = :perguntaId OR :perguntaId = 0) "
            + "GROUP BY p.id_pergunta, IF(pr.texto_resposta is null, r.texto, "
            + "pr.texto_resposta) "
            + " ORDER BY p.id_pergunta, quantidade desc", nativeQuery = true)
    List<Object[]> findQtdRespostaDasPerguntasDoQuestionario(
            @Param("questionarioId") Integer questionarioId,
            @Param("perguntaId") Long perguntaId);

}
