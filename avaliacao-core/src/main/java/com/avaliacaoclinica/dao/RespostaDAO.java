package com.avaliacaoclinica.dao;

import com.avaliacaoclinica.model.RespostaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/29/17 6:33 PM
 */
@Repository
public interface RespostaDAO extends JpaRepository<RespostaEntity, Long> {

}
