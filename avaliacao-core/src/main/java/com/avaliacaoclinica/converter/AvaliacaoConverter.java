package com.avaliacaoclinica.converter;


import com.avaliacaoclinica.model.AvaliacaoEntity;
import com.avaliacaoclinica.model.AvaliadoEntity;
import com.avaliacaoclinica.model.GrupoAtendimentoEnum;
import com.avaliacaoclinica.model.PerguntaEntity;
import com.avaliacaoclinica.model.QuestionarioEntity;
import com.avaliacaoclinica.model.RespostaEntity;
import com.avaliacaoclinica.model.RoleEnum;
import com.avaliacaoclinica.model.SexoEnum;
import com.avaliacaoclinica.model.UsuarioEntity;
import com.avaliacaoclinica.util.DateUtil;
import com.avaliacaoclinica.vo.AvaliacaoVO;
import com.avaliacaoclinica.vo.AvaliadoVO;
import com.avaliacaoclinica.vo.NovoUsuarioVO;
import com.avaliacaoclinica.vo.PerguntaVO;
import com.avaliacaoclinica.vo.QuestionarioVO;
import com.avaliacaoclinica.vo.RespostaVO;
import com.avaliacaoclinica.vo.RoleVO;
import com.avaliacaoclinica.vo.SimplesQuestionarioVO;
import com.avaliacaoclinica.vo.TipoPerguntaVO;
import com.avaliacaoclinica.vo.UsuarioVO;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gustavo on 01/04/17.
 */
public final class AvaliacaoConverter {

    private AvaliacaoConverter() {

    }

    public static UsuarioEntity toUsuarioEntity(final UsuarioVO vo) {
        if (vo == null) {
            return null;
        }
        UsuarioEntity usuarioEntity = new UsuarioEntity();
        usuarioEntity.setId(vo.getId());
        usuarioEntity.setNome(vo.getNome());
        usuarioEntity.setLogin(vo.getLogin());
        usuarioEntity.setSenha(vo.getSenha());
        if (vo.getRole() != null) {
            usuarioEntity.setRole(RoleEnum.valueOf(vo
                    .getRole().name()));
        }
        return usuarioEntity;
    }

    public static UsuarioEntity toUsuarioEntity(final NovoUsuarioVO vo) {
        if (vo == null) {
            return null;
        }
        UsuarioEntity usuarioEntity = new UsuarioEntity();
        if (vo.getId() != null && vo.getId() > 0) {
            usuarioEntity.setId(vo.getId());
        }
        usuarioEntity.setNome(vo.getNome());
        usuarioEntity.setLogin(vo.getLogin());
        usuarioEntity.setSenha(vo.getSenha());
        if (StringUtils.isEmpty(vo.getRole())) {
            usuarioEntity.setRole(RoleEnum.ROLE_USER);
        } else {
            usuarioEntity.setRole(RoleEnum.valueOf(vo.getRole()));
        }
        return usuarioEntity;
    }

    public static UsuarioVO toUsuarioVO(final UsuarioEntity usuarioEntity) {
        UsuarioVO vo = new UsuarioVO();
        vo.setId(usuarioEntity.getId());
        vo.setNome(usuarioEntity.getNome());
        vo.setLogin(usuarioEntity.getLogin());
        vo.setSenha(usuarioEntity.getSenha());
        if (usuarioEntity.getRole() != null) {
            vo.setRole(RoleVO.valueOf(usuarioEntity.getRole().name()));
        }
        return vo;
    }

    public static SimplesQuestionarioVO toSimplesQuestionarioVO(
            final QuestionarioEntity questionarioEntity) {
        if (questionarioEntity == null) {
            return null;
        }
        SimplesQuestionarioVO questionarioVO = new SimplesQuestionarioVO();
        questionarioVO.setId(questionarioEntity.getId());
        questionarioVO.setTitulo(questionarioEntity.getTitulo());
        questionarioVO.setCountAvaliacoes(questionarioEntity.getAvaliacoes()
                .size());
        return questionarioVO;
    }

    public static QuestionarioVO toQuestionarioVO(final QuestionarioEntity
                                                          questionarioEntity) {
        if (questionarioEntity == null) {
            return null;
        }

        QuestionarioVO questionarioVO = new QuestionarioVO();
        questionarioVO.setId(questionarioEntity.getId());
        questionarioVO.setTitulo(questionarioEntity.getTitulo());
        if (questionarioEntity.getAvaliacoes() != null) {
            questionarioVO.setCountAvaliacoes(questionarioEntity
                    .getAvaliacoes().size());
        }

        List<PerguntaVO> perguntasVO = new ArrayList<PerguntaVO>();
        for (PerguntaEntity perguntaEntity : questionarioEntity.getPerguntas()
                ) {
            perguntasVO.add(toPerguntaVO(perguntaEntity));
        }
        questionarioVO.setPerguntas(perguntasVO);
        return questionarioVO;
    }

    public static PerguntaVO toPerguntaVO(final PerguntaEntity perguntaEntity) {
        if (perguntaEntity == null) {
            return null;
        }
        PerguntaVO perguntaVO = new PerguntaVO();
        perguntaVO.setId(perguntaEntity.getId());
        perguntaVO.setPosicao(perguntaEntity.getPosicao());
        perguntaVO.setTexto(perguntaEntity.getTexto());
        perguntaVO.setTipo(TipoPerguntaVO.valueOf(perguntaEntity.getTipo()
                .toString()));
        if (perguntaEntity.getQuestionarioEntity() != null) {
            perguntaVO.setQuestionarioId(
                    perguntaEntity.getQuestionarioEntity().getId());
        }

        if (perguntaEntity.getRespostas() != null && !perguntaEntity
                .getRespostas().isEmpty()) {
            List<RespostaVO> respostasVO = new ArrayList<RespostaVO>();
            for (RespostaEntity respostaEntity : perguntaEntity.getRespostas()
                    ) {
                respostasVO.add(toRespostaVO(respostaEntity));
            }
            perguntaVO.setRespostas(respostasVO);
        }
        return perguntaVO;
    }

    public static RespostaVO toRespostaVO(final RespostaEntity respostaEntity) {
        if (respostaEntity == null) {
            return null;
        }
        RespostaVO respostaVO = new RespostaVO();
        respostaVO.setId(respostaEntity.getId());
        respostaVO.setPosicao(respostaEntity.getPosicao());
        respostaVO.setTexto(respostaEntity.getTexto());
        return respostaVO;
    }

    public static AvaliacaoVO toAvaliacaoVO(final AvaliacaoEntity
                                                    avaliacaoEntity) {
        if (avaliacaoEntity == null) {
            return null;
        }
        AvaliacaoVO avaliacaoVO = new AvaliacaoVO();
        avaliacaoVO.setData(DateUtil.getTimestampAsString(avaliacaoEntity
                .getData()));
        avaliacaoVO.setIdAvaliacao(avaliacaoEntity.getId());
        avaliacaoVO.setIdQuestionario(avaliacaoEntity.getQuestionarioEntity()
                .getId());
        avaliacaoVO.setNomeAvaliado(avaliacaoEntity.getAvaliadoEntity()
                .getNomeAvaliado());
        avaliacaoVO.setIdAvaliado(avaliacaoEntity.getAvaliadoEntity().getId());
        avaliacaoVO.setNomeQuestionario(avaliacaoEntity.getQuestionarioEntity()
                .getTitulo());

        return avaliacaoVO;
    }

    public static AvaliadoEntity toAvaliadoEntity(final AvaliadoVO avaliadoVO) {
        if (avaliadoVO == null) {
            return null;
        }
        AvaliadoEntity avaliadoEntity = new AvaliadoEntity();
        avaliadoEntity.setId(avaliadoVO.getId());
        avaliadoEntity.setNomeAvaliado(avaliadoVO.getNome());
        avaliadoEntity.setCpf(avaliadoVO.getCpf());
        avaliadoEntity.setIdentificadorAtendimento(avaliadoVO
                .getIdentificadorAtendimento());
        avaliadoEntity.setSexo(SexoEnum.parse(avaliadoVO.getSexoAbreviado()));
        avaliadoEntity.setNascimento(DateUtil.getDateAsCalendar(avaliadoVO
                .getNascimento()));
        avaliadoEntity.setGrupoAtendimento(GrupoAtendimentoEnum.parse(
                avaliadoVO.getGrupoAtendimentoId()));
        return avaliadoEntity;
    }

    public static AvaliadoVO toAvaliadoVO(final AvaliadoEntity avaliadoEntity) {
        AvaliadoVO avaliadoVO = new AvaliadoVO();
        avaliadoVO.setId(avaliadoEntity.getId());
        avaliadoVO.setNome(avaliadoEntity.getNomeAvaliado());
        avaliadoVO.setCpf(avaliadoEntity.getCpf());
        avaliadoVO.setIdentificadorAtendimento(avaliadoEntity
                .getIdentificadorAtendimento());
        avaliadoVO.setSexoAbreviado(avaliadoEntity.getSexoAbreviado());
        avaliadoVO.setNascimento(DateUtil.getDateAsString(avaliadoEntity
                .getNascimento()));
        avaliadoVO.setGrupoAtendimentoId(avaliadoEntity.getGrupoAtendimentoId()
        );
        return avaliadoVO;
    }
}
