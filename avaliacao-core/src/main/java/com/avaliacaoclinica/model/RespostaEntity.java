package com.avaliacaoclinica.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by gustavo on 23/09/17.
 */

@Data
@Entity
@Table(name = "Resposta")
@NoArgsConstructor
@ToString
public class RespostaEntity {

    @Id
    @Column(name = "id_resposta")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "texto")
    private String texto;

    @Column(name = "peso")
    private Double peso;

    /**
     * Refere-se a posição que a resposta irá aparecer na pergunta. Ex: a, b,
     * c, d etc.
     */
    @Column(name = "posicao")
    private Integer posicao;

    @ManyToOne
    @JoinColumn(name = "id_pergunta", nullable = false)
    private PerguntaEntity perguntaEntity;

    public RespostaEntity(final Long id, final String texto, final Integer
            posicao) {
        this.id = id;
        this.texto = texto;
        this.posicao = posicao;
    }
}
