package com.avaliacaoclinica.model;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 11/6/17 11:05 AM
 */
public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER;
}
