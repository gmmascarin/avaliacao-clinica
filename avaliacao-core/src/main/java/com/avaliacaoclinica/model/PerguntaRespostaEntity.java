package com.avaliacaoclinica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/29/17 5:42 PM
 */
@Data
@NoArgsConstructor
@ToString
@Entity
@Table(name = "pergunta_resposta")
public class PerguntaRespostaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pergunta_resposta")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_pergunta", nullable = false)
    private PerguntaEntity perguntaEntity;

    @ManyToOne
    @JoinColumn(name = "id_resposta", nullable = true)
    private RespostaEntity respostaEntity;

    @Column(name = "texto_resposta")
    private String texto;

    @ManyToOne
    @JoinColumn(name = "id_avaliacao")
    private AvaliacaoEntity avaliacaoEntity;
}
