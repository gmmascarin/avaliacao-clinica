package com.avaliacaoclinica.model;

public enum SexoEnum {
    MASCULINO('M'), FEMININO('F');

    private Character abreviacao;

    SexoEnum(final Character abreviacao) {
        this.abreviacao = abreviacao;
    }

    public Character getAbreviacao() {
        return abreviacao;
    }

    public static SexoEnum parse(final Character abreviacao) {
        if (abreviacao == null) {
            return null;
        }
        for (SexoEnum sexoEnum : SexoEnum.values()) {
            if (sexoEnum.getAbreviacao().equals(abreviacao)) {
                return sexoEnum;
            }
        }
        return null;
    }
}
