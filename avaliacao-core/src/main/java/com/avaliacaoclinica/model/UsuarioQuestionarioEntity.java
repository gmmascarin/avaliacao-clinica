package com.avaliacaoclinica.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "usuario_questionario")
public class UsuarioQuestionarioEntity {

    @Id
    @Column(name = "id_usuario_questionario")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private UsuarioEntity usuarioEntity;

    @ManyToOne
    @JoinColumn(name = "id_questionario")
    private QuestionarioEntity questionarioEntity;

    @Override
    public String toString() {
        return "UsuarioQuestionarioEntity{"
                + "id=" + id
                + ", usuarioEntity=" + usuarioEntity
                + ", questionarioEntity=" + questionarioEntity
                + '}';
    }
}
