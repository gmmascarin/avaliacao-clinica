package com.avaliacaoclinica.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Calendar;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 8:06 PM
 */
@Data
@NoArgsConstructor
@ToString
@Entity
@Table(name = "Avaliado")
public class AvaliadoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_avaliado")
    private Long id;

    @Column(name = "nome")
    private String nomeAvaliado;

    @Column(name = "cpf")
    private String cpf;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "avaliadoEntity")
    private List<AvaliacaoEntity> avaliacoes;

    @Column(name = "id")
    private String identificadorAtendimento;

    @Column(name = "sexo")
    private Character sexoAbreviado;

    @Column(name = "nascimento")
    private Calendar nascimento;

    @Column(name = "grupoAtendimento")
    private Integer grupoAtendimentoId;

    public GrupoAtendimentoEnum getGrupoAtendimento() {
        return GrupoAtendimentoEnum.parse(grupoAtendimentoId);
    }

    public void setGrupoAtendimento(final GrupoAtendimentoEnum
                                            grupoAtendimento) {
        if (grupoAtendimento != null) {
            this.grupoAtendimentoId = grupoAtendimento.getId();
        }
    }

    public SexoEnum getSexo() {
        return SexoEnum.parse(sexoAbreviado);
    }

    public void setSexo(final SexoEnum sexo) {
        if (sexo != null) {
            this.sexoAbreviado = sexo.getAbreviacao();
        }
    }
}
