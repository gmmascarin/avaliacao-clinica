package com.avaliacaoclinica.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by gustavo on 23/09/17.
 */

@Data
@Entity
@Table(name = "Pergunta")
@NoArgsConstructor
@ToString
public class PerguntaEntity {

    @Id
    @Column(name = "id_pergunta")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String texto;

    @Enumerated(EnumType.STRING)
    private TipoPergunta tipo;

    @Column(name = "expressao")
    private String expressao;

    @ManyToOne
    @JoinColumn(name = "id_questionario", nullable = false)
    private QuestionarioEntity questionarioEntity;

    /**
     * Refere-se a posição que a pergunta irá aparecer no questionário. Ex:
     * 1, 2, 3 etc.
     */
    @Column(name = "posicao")
    private Integer posicao;

    /**
     * Somente terão respostas as perguntas de múltipla escolha
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "perguntaEntity", cascade =
            {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    private List<RespostaEntity> respostas;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "perguntaEntity")
    private List<PerguntaRespostaEntity> perguntasRespostas;

    public PerguntaEntity(
            final Long id, final TipoPergunta tipo, final String texto, final
    Integer
            posicao) {
        this.id = id;
        this.texto = texto;
        this.posicao = posicao;
        this.tipo = tipo;
    }

    public void setRespostas(final List<RespostaEntity> respostas) {
        if (this.tipo.equals(TipoPergunta.MULTIPLA_ESCOLHA)) {
            this.respostas = respostas;
        }
    }

    public QuestionarioEntity getQuestionarioEntity() {
        return questionarioEntity;
    }
}
