package com.avaliacaoclinica.model;

import java.util.Objects;

public enum GrupoAtendimentoEnum {
    FISIOTERAPIA(1, "Fisioterapia");

    private Integer id;
    private String nome;

    GrupoAtendimentoEnum(final Integer id, final String nome) {
        this.id = id;
        this.nome = nome;
    }

    public static GrupoAtendimentoEnum parse(final Integer id) {
        if (id == null) {
            return null;
        }
        for (GrupoAtendimentoEnum grupoAtendimentoEnum : GrupoAtendimentoEnum
                .values()) {
            if (Objects.equals(grupoAtendimentoEnum.getId(), id)) {
                return grupoAtendimentoEnum;
            }
        }
        return null;
    }

    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
}
