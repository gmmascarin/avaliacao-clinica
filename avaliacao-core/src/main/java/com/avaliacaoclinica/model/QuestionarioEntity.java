package com.avaliacaoclinica.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by gustavo on 23/09/17.
 */

@Data
@Entity
@Table(name = "Questionario")
@NoArgsConstructor
public class QuestionarioEntity {

    public QuestionarioEntity(final Integer id, final String titulo) {
        this.id = id;
        this.titulo = titulo;
    }

    public QuestionarioEntity(final Integer id) {
        this.id = id;
    }

    @Id
    @Column(name = "id_questionario")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "titulo")
    private String titulo;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "questionarioEntity",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType
                    .REMOVE})
    private List<PerguntaEntity> perguntas;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "questionarioEntity",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType
                    .REMOVE})
    private List<UsuarioQuestionarioEntity> usuarioQuestionarios;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "questionarioEntity")
    private List<AvaliacaoEntity> avaliacoes;

    @Override
    public String toString() {
        return titulo;
    }
}
