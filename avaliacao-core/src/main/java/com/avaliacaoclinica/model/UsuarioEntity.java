package com.avaliacaoclinica.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by gustavo on 24/12/16.
 */
@Data
@Entity
@Table(name = "Usuario")
@NoArgsConstructor
public class UsuarioEntity {

    public UsuarioEntity(final Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "login")
    private String login;

    @Column(name = "senha")
    private String senha;

    @Enumerated(EnumType.STRING)
    private RoleEnum role;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuarioEntity", cascade =
            {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    private List<UsuarioQuestionarioEntity> usuarioQuestionarios;

    @Override
    public String toString() {
        return "UsuarioEntity{"
                + "id=" + id
                + ", nome='" + nome + '\''
                + ", login='" + login + '\''
                + ", role=" + role
                + '}';
    }
}
