package com.avaliacaoclinica.model;

public enum TipoPergunta {
    TEXTO, MULTIPLA_ESCOLHA, EXPRESSAO;
}
