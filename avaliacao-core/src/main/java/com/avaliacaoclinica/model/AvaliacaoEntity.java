package com.avaliacaoclinica.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Calendar;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 8:24 PM
 */
@Data
@NoArgsConstructor
@ToString
@Entity
@Table(name = "avaliacao")
public class AvaliacaoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_avaliacao")
    private Long id;

    @Column(name = "data")
    private Calendar data;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "avaliacaoEntity", cascade
            = CascadeType.ALL)
    private List<PerguntaRespostaEntity> listPerguntaRespostaEntity;

    @ManyToOne
    @JoinColumn(name = "id_avaliado", nullable = false)
    private AvaliadoEntity avaliadoEntity;

    @ManyToOne
    @JoinColumn(name = "id_questionario")
    private QuestionarioEntity questionarioEntity;

}
