package com.avaliacaoclinica.util;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 7:03 PM
 */
@Slf4j
public final class DateUtil {

    private DateUtil() {
    }

    public enum PatternType {
        TIMESTAMP("yyyy-MM-dd HH:mm:ss"), DATE("dd/MM/yyyy");
        private String pattern;

        PatternType(final String pattern) {
            this.pattern = pattern;
        }

        public String getPattern() {
            return pattern;
        }
    }

    public static String getDateAsString(final Calendar date) {
        if (date != null) {
            return getDateAsString(date, PatternType.DATE);
        }
        return null;
    }

    public static String getTimestampAsString(final Calendar date) {
        if (date != null) {
            return getDateAsString(date, PatternType.TIMESTAMP);
        }
        return null;
    }

    public static Calendar getDateAsCalendar(final String date) {
        if (date != null) {
            return getDateAsCalendar(date, PatternType.DATE);
        }
        return null;
    }

    public static Calendar getTimestampAsCalendar(final String date) {
        if (date != null) {
            return getDateAsCalendar(date, PatternType.TIMESTAMP);
        }
        return null;
    }

    private static Calendar getDateAsCalendar(final String date,
                                              final PatternType patternType) {
        if (date != null) {
            final Calendar calendar = Calendar.getInstance();
            try {
                SimpleDateFormat formatter = new SimpleDateFormat(
                        patternType.getPattern());
                calendar.setTime(formatter.parse(date));
                return calendar;
            } catch (ParseException e) {
                log.error(e.getMessage());
            }
        }
        return null;
    }

    private static String getDateAsString(final Calendar date,
                                          final PatternType patternType) {
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat(
                    patternType.getPattern());
            return formatter.format(date.getTime());
        }
        return null;
    }
}
