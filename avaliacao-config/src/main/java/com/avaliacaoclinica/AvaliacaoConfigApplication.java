package com.avaliacaoclinica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
@SpringBootApplication
public class AvaliacaoConfigApplication {

    public static void main(final String[] args) {
        SpringApplication.run(AvaliacaoConfigApplication.class, args);
    }
}
