package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.exception.AvaliadoJaCadastradoException;
import com.avaliacaoclinica.model.GrupoAtendimentoEnum;
import com.avaliacaoclinica.service.AvaliadoService;
import com.avaliacaoclinica.vo.AvaliadoVO;
import com.avaliacaoclinica.vo.GrupoAtendimentoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 9:11 PM
 */
@RestController
@Api(value = "/avaliado", description = "Gerenciamento de Avaliados")
@Slf4j
public class AvaliadoController {

    @Autowired
    private AvaliadoService avaliadoService;

    @PostMapping("/avaliados")
    @ApiOperation(value = "Insere avaliado")
    public ResponseEntity<Boolean> salvarAvaliado(
            @RequestBody @Valid final
            AvaliadoVO avaliadoVO,
            final BindingResult
                    processErrors) {
        if (processErrors.hasErrors()) {
            return new ResponseEntity(Boolean.FALSE, HttpStatus
                    .BAD_REQUEST);
        }
        log.info("M=salvarAvaliado, avaliadoVO=" + avaliadoVO);
        Boolean isSuccess = Boolean.FALSE;
        try {
            isSuccess = avaliadoService.salvarAvaliado(avaliadoVO);
        } catch (AvaliadoJaCadastradoException e) {
            log.error("AvaliadoJaCadastradoException", e);
        }

        if (!isSuccess) {
            log.info("M=salvarAvaliado, msg=notfound");
            return new ResponseEntity(isSuccess, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(Boolean.TRUE, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value =
            "/questionarios/{id}/avaliados")
    @ApiOperation(value = "Busca as perguntas da avaliação")
    public ResponseEntity<List<AvaliadoVO>> buscarAvaliadosPorQuestionarioId(
            @PathVariable(value = "id", required = true) final Integer
                    questionarioId) {
        log.info("M=buscarAvaliadosPorQuestionarioId, questionarioId="
                + questionarioId);
        List<AvaliadoVO> avaliados = avaliadoService
                .buscarAvaliadosPorQuestionarioId(questionarioId);

        if (avaliados == null || avaliados.isEmpty()) {
            log.info("M=buscarAvaliadosPorQuestionarioId, msg=notfound");
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(avaliados, HttpStatus.OK);
    }

    @GetMapping(value = "/avaliados/grupoAtendimento")
    public List<GrupoAtendimentoVO> getAllGrupoAtendimento() {
        List<GrupoAtendimentoVO> grupos = new ArrayList<>();
        for (GrupoAtendimentoEnum grupo : GrupoAtendimentoEnum.values()) {
            grupos.add(new GrupoAtendimentoVO(grupo.getId(), grupo.getNome()));
        }
        return grupos;
    }
}
