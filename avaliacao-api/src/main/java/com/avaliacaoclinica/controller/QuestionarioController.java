package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.exception.PerguntaException;
import com.avaliacaoclinica.exception.QuestionarioCantBeDeletedException;
import com.avaliacaoclinica.exception.QuestionarioNotFoundException;
import com.avaliacaoclinica.service.QuestionarioService;
import com.avaliacaoclinica.vo.ControleAcessoQuestionarioVO;
import com.avaliacaoclinica.vo.NovaPerguntaVO;
import com.avaliacaoclinica.vo.QuestionarioVO;
import com.avaliacaoclinica.vo.SimplesQuestionarioVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by gustavo on 24/09/17.
 */
@RestController()
@Api(value = "/questionarios", description = "Gerenciamento de questionários")
@Slf4j
public class QuestionarioController {

    @Autowired
    private QuestionarioService questionarioService;

    @RequestMapping(method = RequestMethod.GET, value =
            "/usuarios/{id}/questionarios/all")
    @ApiOperation(value = "Busca as avaliações do usuário, retornando também "
            + "as perguntas e respostas.")
    public ResponseEntity<List<QuestionarioVO>> getQuestionariosCompletos(
            @PathVariable(value = "id", required = true) final Long
                     usuarioId) {
        log.info("M=getQuestionariosCompletos, usuarioId=" + usuarioId);
        final List<QuestionarioVO> questionarios = questionarioService
                .findAllQuestionariosCompletosByIdUsuario(usuarioId);

        if (questionarios == null || questionarios.isEmpty()) {
            log.info("M=getQuestionariosCompletos, msg=notfound");
            return new ResponseEntity<List<QuestionarioVO>>(HttpStatus
                    .NOT_FOUND);
        }
        return new ResponseEntity<List<QuestionarioVO>>(questionarios,
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value =
            "/usuarios/{id}/questionarios")
    @ApiOperation(value = "Busca as avaliações do usuário sem as perguntas e "
            + "respostas")
    public ResponseEntity<List<SimplesQuestionarioVO>> getQuestionarios(
            @PathVariable(value = "id", required = true) final Long usuarioId) {
        log.info("M=getQuestionarios, usuarioId=" + usuarioId);
        final List<SimplesQuestionarioVO> questionarios = questionarioService
                .findsAllQuestionarioByIdUsuario(usuarioId);
        return ResponseEntity.ok(questionarios);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/questionarios")
    public ResponseEntity<QuestionarioVO> salvarQuestionario(
            @RequestBody final NovaPerguntaVO novaPerguntaVO)
            throws QuestionarioNotFoundException, PerguntaException {
        log.info("M=salvarQuestionario, novaPerguntaVO=" + novaPerguntaVO);
        QuestionarioVO questionarioVO = questionarioService
                .salvarQuestionario(novaPerguntaVO);

        if (questionarioVO == null) {
            log.info("M=salvarQuestionario, msg=nao criado");
            return new ResponseEntity<QuestionarioVO>(HttpStatus
                    .INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<QuestionarioVO>(questionarioVO, HttpStatus
                .CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/questionarios/{id}")
    public ResponseEntity<QuestionarioVO> getQuestionarioPorId(
            @PathVariable(value = "id") final Integer questionarioId) {
        log.info("M=getQuestionarioPorId, questionarioId=" + questionarioId);
        QuestionarioVO questionarioVO = questionarioService.findById(
                questionarioId);

        if (questionarioVO == null) {
            log.info("M=getQuestionarioPorId, msg=nao criado");
            return new ResponseEntity<QuestionarioVO>(HttpStatus
                    .INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<QuestionarioVO>(questionarioVO, HttpStatus
                .CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value =
            "/questionarios/{id}")
    @ApiOperation(value = "Exclui um questionário", response = Boolean.class)
    public ResponseEntity<Boolean> excluirQuestionario(
            @PathVariable(value = "id") final Integer questionarioId)
            throws QuestionarioCantBeDeletedException {
        log.info("M=excluirAvaliacao, questionarioId=" + questionarioId);
        Boolean success = questionarioService.delete(questionarioId);
        if (success) {
            log.info("M=excluirQuestionario, success");
            return new ResponseEntity<Boolean>(success, HttpStatus.OK);
        } else {
            log.info("M=excluirQuestionario, not success");
            return new ResponseEntity<Boolean>(success, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/questionarios/{questionarioId}/controleAcesso")
    @ApiOperation(value = "Lista os usuários que tem permissão de acesso no "
            + "questionário", response = ControleAcessoQuestionarioVO.class)
    public ResponseEntity<ControleAcessoQuestionarioVO> getControleAcesso(
            @PathVariable(value = "questionarioId", required = true) final
             Integer questionarioId) {
        log.info("M=getControleAcesso, questionarioID=" + questionarioId);
        ControleAcessoQuestionarioVO controleAcesso = questionarioService
                .getControleAcesso(questionarioId);
        log.info("M=getControleAcesso, controleAcesso=" + controleAcesso);
        if (controleAcesso == null) {
            return new ResponseEntity<ControleAcessoQuestionarioVO>(
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<ControleAcessoQuestionarioVO>(
                controleAcesso, HttpStatus.OK);
    }

    @PostMapping(value = "/questionarios/controleAcesso")
    @ApiOperation(value = "Altera os usuários que poderão acessar o "
            + "questionario", response = ControleAcessoQuestionarioVO.class)
    public ResponseEntity<ControleAcessoQuestionarioVO>
    atualizarControleAcesso(
            @RequestBody @Valid final ControleAcessoQuestionarioVO
                    controleAcessoQuestionarioVO) {
        log.info("M=atualizarControleAcesso, controleAcessoQuestionarioVO="
                 + controleAcessoQuestionarioVO);
        ControleAcessoQuestionarioVO controleAcessoQuestionarioVOAtualizado =
                questionarioService.updateControleAcesso(
                        controleAcessoQuestionarioVO);
        log.info("M=getControleAcesso, "
                + "controleAcessoQuestionarioVOAtualizado="
                + controleAcessoQuestionarioVOAtualizado);
        if (controleAcessoQuestionarioVO == null) {
            return new ResponseEntity<ControleAcessoQuestionarioVO>(
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<ControleAcessoQuestionarioVO>(
                controleAcessoQuestionarioVOAtualizado, HttpStatus.OK);
    }
}
