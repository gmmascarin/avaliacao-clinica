package com.avaliacaoclinica.controller;

import org.springframework.validation.BindingResult;

/**
 * Created by gustavo on 01/04/17.
 */
public class AbstractController {

    protected String getErrorMessages(final BindingResult validationResult) {
        StringBuilder sb = new StringBuilder();
        if (validationResult.hasErrors()) {
            validationResult.getFieldErrors()
                    .forEach(e -> {
                        if (sb.length() == 0) {
                            sb.append("").append(e.getField()).append("=")
                                    .append(e.getDefaultMessage());
                        } else {
                            sb.append(" | ").append(e.getField()).append("=")
                                    .append(e.getDefaultMessage());
                        }
                    });
        }
        return sb.toString();
    }
}
