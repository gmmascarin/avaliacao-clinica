package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.exception.PerguntaCantBeDeletedException;
import com.avaliacaoclinica.service.PerguntaService;
import com.avaliacaoclinica.vo.PerguntaVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by gustavo on 24/09/17.
 */
@RestController()
@Api(value = "/perguntas", description = "Gerenciamento de Perguntas")
@Slf4j
public class PerguntaController {

    @Autowired
    private PerguntaService perguntaService;

    @RequestMapping(method = RequestMethod.GET, value =
            "/questionarios/{id}/perguntas")
    @ApiOperation(value = "Busca as perguntas da avaliação")
    public ResponseEntity<List<PerguntaVO>> getPerguntas(@PathVariable(value
            = "id", required = true) final Integer questionarioId) {
        log.info("M=getPerguntas, questionarioId=" + questionarioId);
        List<PerguntaVO> perguntas = perguntaService.findAllByQuestionarioId(
                questionarioId);

        if (perguntas == null || perguntas.isEmpty()) {
            log.info("M=getPerguntas, msg=notfound");
            return new ResponseEntity<List<PerguntaVO>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<PerguntaVO>>(perguntas, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/perguntas/{id}")
    @ApiOperation(value = "Busca uma pergunta")
    public ResponseEntity<PerguntaVO> getPergunta(@PathVariable(value = "id",
            required = true) final Long perguntaId) {
        log.info("M=getPergunta, perguntaId=" + perguntaId);
        PerguntaVO pergunta = perguntaService.findOne(perguntaId);

        if (pergunta == null) {
            log.info("M=getPergunta, msg=notfound");
            return new ResponseEntity<PerguntaVO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<PerguntaVO>(pergunta, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/perguntas/{id}")
    @ApiOperation(value = "Exclui uma pergunta")
    public ResponseEntity<Boolean> excluirPergunta(@PathVariable(value =
            "id", required = true) final Long perguntaId) throws
            PerguntaCantBeDeletedException {
        log.info("M=excluirPergunta, perguntaId=" + perguntaId);
        Boolean success = perguntaService.delete(perguntaId);
        if (success) {
            log.info("M=excluirPergunta, success");
            return new ResponseEntity<Boolean>(success, HttpStatus.OK);
        } else {
            log.info("M=excluirPergunta, not success");
            return new ResponseEntity<Boolean>(success, HttpStatus.NOT_FOUND);
        }
    }
}
