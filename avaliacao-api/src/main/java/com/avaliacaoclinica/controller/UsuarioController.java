package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.converter.AvaliacaoConverter;
import com.avaliacaoclinica.dao.UsuarioDAO;
import com.avaliacaoclinica.model.UsuarioEntity;
import com.avaliacaoclinica.vo.LoginVO;
import com.avaliacaoclinica.vo.NovoUsuarioVO;
import com.avaliacaoclinica.vo.UsuarioVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gustavo on 24/12/16.
 */
@RestController
@Api(value = "/usuarios", description = "Gerenciamento de usuários")
@Slf4j
public class UsuarioController extends AbstractController {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final int PASSWORD_MAX_LENGTH = 20;

    @RequestMapping(method = RequestMethod.GET, value = "/usuarios")
    @ApiOperation(value = "Lista os usuários")
    public ResponseEntity<List<UsuarioVO>> findAll() {
        log.info("M=findAll");
        List<UsuarioEntity> usuarios = usuarioDAO.findAll();
        if (usuarios == null || usuarios.isEmpty()) {
            log.info("M=findAll, msg=notfound");
            return new ResponseEntity<List<UsuarioVO>>(HttpStatus.NOT_FOUND);
        }
        List<UsuarioVO> usuarioVOS = new ArrayList<>();
        for (final UsuarioEntity usuario : usuarios) {
            usuarioVOS.add(AvaliacaoConverter.toUsuarioVO(usuario));
        }

        return new ResponseEntity<List<UsuarioVO>>(usuarioVOS, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/usuarios/{id}")
    @ApiOperation(value = "Lista usuário")
    public ResponseEntity<UsuarioVO> getUser(
            @PathVariable(value = "id") final Integer usuarioId) {
        log.info("M=getUser, usuarioId=" + usuarioId);
        UsuarioEntity usuario = usuarioDAO.findOne(usuarioId.longValue());
        if (usuario == null) {
            log.info("M=getUser, msg=notfound");
            return new ResponseEntity<UsuarioVO>(HttpStatus.NOT_FOUND);
        }
        log.info("M=getUser, user=" + usuario);
        return new ResponseEntity<UsuarioVO>(AvaliacaoConverter.
                toUsuarioVO(usuario), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value =
            "/usuarios/login/{login}")
    @ApiOperation(value = "Lista usuário")
    public ResponseEntity<UsuarioVO> getUser(
            @PathVariable(value = "login") final String login) {
        log.info("M=getUser, login=" + login);
        UsuarioEntity usuario = usuarioDAO.findByLogin(login);
        if (usuario == null) {
            log.info("M=getUser, msg=notfound");
            return new ResponseEntity<UsuarioVO>(HttpStatus.NOT_FOUND);
        }
        log.info("M=getUser, user=" + usuario);
        return new ResponseEntity<UsuarioVO>(
                AvaliacaoConverter.toUsuarioVO(usuario), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/usuarios/login")
    @ApiOperation(value = "Login")
    public ResponseEntity<UsuarioVO> login(@RequestBody @Valid final LoginVO
                                                   loginVO, final
                                           BindingResult resul) {
        log.info("M=login, loginVO=" + loginVO);

        if (resul.hasErrors()) {
            log.error("M=login, msg=parameters incorrect",
                    resul.getAllErrors());
            return new ResponseEntity<UsuarioVO>(HttpStatus.BAD_REQUEST);
        }
        UsuarioEntity usuario = usuarioDAO.findByLogin(loginVO.getUsuario());
        if (usuario == null || !passwordEncoder.matches(
                loginVO.getSenha(),
                usuario.getSenha())) {
            log.info("M=login, msg=incorrect");
            return new ResponseEntity<UsuarioVO>(HttpStatus.NOT_FOUND);
        }
        log.info("M=login, user=" + usuario);
        return new ResponseEntity<UsuarioVO>(
                AvaliacaoConverter.toUsuarioVO(usuario), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/usuarios")
    @ApiOperation(value = "Criar novo usuário")
    public ResponseEntity<UsuarioVO> createUser(@RequestBody @Valid final
                                                NovoUsuarioVO vo, final
                                                BindingResult
                                                        validationResult) {
        log.info("M=createUser, vo=" + vo);

        if (vo == null || validationResult.hasErrors()) {
            log.info("M=createUser, error="
                    + getErrorMessages(validationResult));
            return new ResponseEntity<UsuarioVO>(HttpStatus.BAD_REQUEST);
        } else if (usuarioDAO.findByLogin(vo.getLogin()) != null) {
            log.info("M=createUser, msg=login já existente.");
            return new ResponseEntity<UsuarioVO>(HttpStatus.CONFLICT);
        } else {
            vo.setSenha(passwordEncoder.encode(vo.getSenha()));
            UsuarioEntity usuarioSalvo = usuarioDAO.save(AvaliacaoConverter
                    .toUsuarioEntity(vo));
            log.info("M=createUser, msg=sucesso!");
            return new ResponseEntity<UsuarioVO>(AvaliacaoConverter
                    .toUsuarioVO(usuarioSalvo), HttpStatus.CREATED);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/usuarios")
    @ApiOperation(value = "Criar novo usuário")
    public ResponseEntity<UsuarioVO> updateUser(@RequestBody @Valid final
                                                NovoUsuarioVO vo) {
        log.info("M=updateUser, vo=" + vo);

        if (vo == null || vo.getId() == null) {
            log.info("M=updateUser, error=usuario ou id incorreto");
            return new ResponseEntity<UsuarioVO>(HttpStatus.BAD_REQUEST);
        } else {
            if (vo.getSenha().length() < PASSWORD_MAX_LENGTH) { //senha com
                // mais de 20
                // caracteres significa que já foi criptografada
                vo.setSenha(passwordEncoder.encode(vo.getSenha()));
            }
            UsuarioEntity usuarioSalvo = usuarioDAO.save(AvaliacaoConverter
                    .toUsuarioEntity(vo));
            log.info("M=updateUser, msg=sucesso!");
            return new ResponseEntity<UsuarioVO>(AvaliacaoConverter
                    .toUsuarioVO(usuarioSalvo), HttpStatus.CREATED);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/usuarios/{id}")
    @ApiOperation(value = "Exclui um usuário", response = Boolean.class)
    public ResponseEntity<Boolean> delete(@PathVariable(value = "id",
            required = true) final Long usuarioId) {
        log.info("M=excluirAvaliacao, usuarioId=" + usuarioId);

        UsuarioEntity usuarioEntity = usuarioDAO.findOne(usuarioId);
        if (usuarioEntity == null) {
            return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus
                    .NOT_FOUND);
        }
        try {
            usuarioDAO.delete(usuarioId);
        } catch (Exception e) {
            return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus
                    .NOT_FOUND);
        }
        return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
    }
}
