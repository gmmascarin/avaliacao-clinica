package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.exception.AvaliadoNotFoundException;
import com.avaliacaoclinica.exception.PerguntaNotFoundException;
import com.avaliacaoclinica.exception.QuestionarioNotFoundException;
import com.avaliacaoclinica.exception.RespostaNotFoundException;
import com.avaliacaoclinica.service.AvaliacaoService;
import com.avaliacaoclinica.vo.AvaliacaoVO;
import com.avaliacaoclinica.vo.EfetivaAvaliacaoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.script.ScriptException;
import javax.validation.Valid;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 6:02 PM
 */
@RestController()
@Api(value = "/avaliacoes", description = "Gerenciamento de avaliações")
@Slf4j
public class AvaliacaoController {

    @Autowired
    private AvaliacaoService avaliacaoService;

    @RequestMapping(method = RequestMethod.POST, value = "/avaliacoes")
    @ApiOperation(value = "Efetivar a avaliação", response = Boolean.class)
    public ResponseEntity<Boolean> avaliar(@RequestBody @Valid
                                                       final EfetivaAvaliacaoVO
                                                       efetivaAvaliacaoVO,
                                           final BindingResult processErrors) {
        log.info("M=avaliar, efetivaAvaliacaoVO=" + efetivaAvaliacaoVO);
        if (processErrors.hasErrors()) {
            return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus
                    .BAD_REQUEST);
        }
        final Boolean isSuccess;
        try {
            isSuccess = avaliacaoService.avaliar(efetivaAvaliacaoVO);
        } catch (PerguntaNotFoundException | RespostaNotFoundException
                | AvaliadoNotFoundException
                | QuestionarioNotFoundException | ScriptException e) {
            log.error(e.getMessage());
            return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus
                    .NOT_FOUND);
        }
        return new ResponseEntity<Boolean>(isSuccess, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value =
            "/usuarios/{id}/avaliacoes")
    @ApiOperation(value = "Busca as avaliações preenchidas pelo usuário")
    public ResponseEntity<List<AvaliacaoVO>> buscarTodasPeloIdUsuario(
            @PathVariable(value = "id", required = true) final Long usuarioId) {
        final List<AvaliacaoVO> avaliacoes = avaliacaoService
                .buscarTodasPeloIdUsuario(usuarioId);
        return new ResponseEntity<List<AvaliacaoVO>>(avaliacoes, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value =
            "/questionarios/{id}/avaliacoes")
    @ApiOperation(value = "Busca as avaliações do questionário")
    public ResponseEntity<List<AvaliacaoVO>> buscarTodasPeloIdUsuario(
            @PathVariable(value = "id", required = true) final Integer
                    questionarioId) {
        log.info("M=buscarTodasPeloIdUsuario, questionarioId="
                + questionarioId);
        final List<AvaliacaoVO> avaliacoes = avaliacaoService
                .buscarTodasPeloIdQuestionario(questionarioId);
        return new ResponseEntity<List<AvaliacaoVO>>(avaliacoes, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/avaliacoes/{id}")
    @ApiOperation(value = "Exclui uma avaliação", response = Boolean.class)
    public ResponseEntity<Boolean> excluirAvaliacao(@PathVariable(value =
            "id", required = true) final Long avaliacaoId) {
        log.info("M=excluirAvaliacao, avaliacaoId=" + avaliacaoId);
        Boolean success = avaliacaoService.delete(avaliacaoId);
        if (success) {
            log.info("M=excluirAvaliacao, success");
            return new ResponseEntity<Boolean>(success, HttpStatus.OK);
        } else {
            log.info("M=excluirAvaliacao, not success");
            return new ResponseEntity<Boolean>(success, HttpStatus.NOT_FOUND);
        }
    }

}
