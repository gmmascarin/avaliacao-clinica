package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.service.HomeService;
import com.avaliacaoclinica.vo.HomeAppVO;
import com.avaliacaoclinica.vo.HomeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/19/17 6:05 PM
 */
@RestController()
@Api(value = "/home", description = "Controllers da pagina inicial")
@Slf4j
public class HomeController {

    @Autowired
    private HomeService homeService;

    @RequestMapping(method = RequestMethod.GET, value = "/home/{id}")
    @ApiOperation(value = "Lista os dados da HOME Web")
    public ResponseEntity<HomeVO> home(@PathVariable(value = "id", required =
            true) final Integer usuarioId) {
        log.info("M=home");
        HomeVO home = homeService.home(usuarioId.longValue());
        if (home == null) {
            log.info("M=home, msg=notfound");
            return new ResponseEntity<HomeVO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<HomeVO>(home, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/homeApp/{id}")
    @ApiOperation(value = "Lista os dados da Home do APP")
    public ResponseEntity<HomeAppVO> homeApp(@PathVariable(value = "id",
            required = true) final Integer usuarioId) {
        log.info("M=homeApp");
        HomeAppVO home = homeService.homeApp(usuarioId.longValue());
        if (home == null) {
            log.info("M=homeApp, msg=notfound");
            return new ResponseEntity<HomeAppVO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<HomeAppVO>(home, HttpStatus.OK);
    }

}
