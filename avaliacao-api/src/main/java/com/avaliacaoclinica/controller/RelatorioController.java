package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.service.AvaliacaoService;
import com.avaliacaoclinica.vo.ExportacaoVO;
import com.avaliacaoclinica.vo.GraficoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@Api(value = "/relatorios", description = "Emiss�o de relatorios")
@Slf4j
public class RelatorioController {

    @Autowired
    private AvaliacaoService avaliacaoService;

    @RequestMapping(method = RequestMethod.GET, value =
            "/relatorios/questionarios/{questionarioId}/avaliacoes"
                    + "/{avaliadoId}/exportacao")
    @ApiOperation(value = "Busca as informaçõess para exportaçãoo de "
            + "relatório de avaliações")
    public ResponseEntity<List<ExportacaoVO>> exportar(
            @PathVariable(value = "questionarioId", required = true) final
            Integer questionarioId, @PathVariable(value = "avaliadoId") final
            Long avaliadoId) {
        log.info("M=exportar, questionarioId=" + questionarioId + ", "
                + "avaliadoId=" + avaliadoId);

        final List<ExportacaoVO> exportacaoVOList = avaliacaoService.export(
                questionarioId, avaliadoId);

        if (exportacaoVOList == null) {
            log.info("M=getQuestionarios, msg=notfound");
            return new ResponseEntity<List<ExportacaoVO>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<ExportacaoVO>>(
                exportacaoVOList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "/relatorios/questionarios/{questionarioId"
            + "}/perguntas/{perguntaId}/graficos", method = RequestMethod.GET)
    @ApiOperation(value = "Buscas as informações para preencher os relatórios"
            + " de gráfico")
    public ResponseEntity<List<GraficoVO>> getGraficosInfo(
            @PathVariable("questionarioId") final Integer questionarioId,
            @PathVariable("perguntaId") final Long perguntaId) {
        log.info("M=getGraficosInfo, questionarioId={}, perguntaId={}",
                questionarioId, perguntaId);

        List<GraficoVO> graficosVO = avaliacaoService.getGraficosInfo(
                questionarioId, perguntaId);

        if (graficosVO == null) {
            log.info("M=getGraficosInfo, msg=notfound");
            return new ResponseEntity<List<GraficoVO>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<GraficoVO>>(graficosVO, HttpStatus.OK);

    }
}
