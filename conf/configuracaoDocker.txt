*****Para criar o container docker
docker run -p 3306:3306 -d -e MYSQL_ROOT_PASSWORD=123456 -v /home/ec2-user/storage/docker/mysql-data:/var/lib/mysql --name container-avaliacao-mysql --restart unless-stopped mysql:latest

*****Para executar o bash dentro do container docker
docker exec -it container-avaliacao-mysql bash

*****Para conectar no mysql
mysql -uroot -p

*****Criar o database
CREATE DATABASE avaliacao_clinica;

*****Para saber o IP da instância docker
docker inspect container-avaliacao-mysql;
procurar por IPAddress.

*******Para criar container phpmyadmin
docker run -d --link container-avaliacao-mysql:db --name phpmyadmin -e MYSQL_USERNAME=root -p 8090:80 phpmyadmin/phpmyadmin

********acessar o phpmyadmin através da url
http://localhost:8090/







*****************************
Depois dos containers criados, para usá-lo:

1-Start o container do Mysql
docker start container-avaliacao-mysql

2-Se precisar executar comandos no banco via terminal:
docker exec -it container-avaliacao-mysql bash

Após está etapa estará conectado no container.
Comandos Mysql:
mysql -uroot -p
password: 123456

use avaliacao_clinica;
show tables;

3-Rodar o phpmyadin
docker run -d --link container-avaliacao-mysql:db --name phpmyadmin -e MYSQL_USERNAME=root -p 8090:80 phpmyadmin/phpmyadmin

4-Jenkins (ficará disponível na porta 8091.)
docker run -d -p 49001:8091 -v ~/jenkins:/var/jenkins_home:z -t jenkins