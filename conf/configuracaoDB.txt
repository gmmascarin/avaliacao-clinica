*****Criar o database
CREATE DATABASE avaliacao_clinica;

*****Criar usuário
CREATE USER 'user_api'@'%' IDENTIFIED BY '123456';
CREATE USER 'user_web'@'%' IDENTIFIED BY '123456';

****Permissões do usuário
GRANT ALL PRIVILEGES ON avaliacao_clinica TO 'user_api'@'%';
GRANT ALL PRIVILEGES ON avaliacao_clinica TO 'user_web'@'%';

GRANT ALL PRIVILEGES ON avaliacao_clinica.* TO 'user_api'@'%';
GRANT ALL PRIVILEGES ON avaliacao_clinica.* TO 'user_web'@'%';


*****Recarrega os privilégios
FLUSH PRIVILEGES;