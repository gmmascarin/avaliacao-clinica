insert into usuario(nome, login, senha, role) values ('Gustavo de Mello Mascarin', 'gustavo', '$2a$10$Xp/E2Mq1BrQvH456bM0eierNa6QV8StWmwMhm07M6jfCn2Dag1E5K', 'ROLE_ADMIN');

insert into questionario(titulo, id_usuario) values ('Teste', 1);

insert into pergunta(texto, posicao, tipo, id_questionario)
values('Qual a capital do Brasil', 1, 'MULTIPLA_ESCOLHA', 1);
insert into resposta(texto, posicao, id_pergunta)
values('São Paulo', 1, 1);
insert into resposta(texto, posicao, id_pergunta)
values('Rio de Janeiro', 2, 1);
insert into resposta(texto, posicao, id_pergunta)
values('Brasília', 3, 1);
insert into resposta(texto, posicao, id_pergunta)
values('Bahia', 4, 1);

insert into pergunta(texto, posicao, tipo, id_questionario)
values('Qual sua idade', 2, 'TEXTO', 1);