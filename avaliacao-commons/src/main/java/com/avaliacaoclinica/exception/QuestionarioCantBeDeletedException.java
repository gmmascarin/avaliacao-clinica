package com.avaliacaoclinica.exception;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/26/17 2:53 PM
 */
public class QuestionarioCantBeDeletedException extends Exception {

    public QuestionarioCantBeDeletedException(final String message) {
        super(message);
    }
}
