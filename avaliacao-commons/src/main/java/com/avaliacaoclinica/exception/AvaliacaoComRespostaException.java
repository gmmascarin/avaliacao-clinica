package com.avaliacaoclinica.exception;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/25/17 12:51 PM
 */
public class AvaliacaoComRespostaException extends Exception {

    public AvaliacaoComRespostaException() {
    }

    public AvaliacaoComRespostaException(final String message) {
        super(message);
    }
}
