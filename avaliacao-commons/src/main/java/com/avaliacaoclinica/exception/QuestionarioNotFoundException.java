package com.avaliacaoclinica.exception;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 8:51 PM
 */
public class QuestionarioNotFoundException extends Exception {

    public QuestionarioNotFoundException() {
        super("Questionario não encontrado");
    }
}
