package com.avaliacaoclinica.exception;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/26/17 3:34 PM
 */
public class AvaliacaoException extends Exception {

    public AvaliacaoException() {
    }

    public AvaliacaoException(final String message) {
        super(message);
    }

    public AvaliacaoException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AvaliacaoException(final Throwable cause) {
        super(cause);
    }

    public AvaliacaoException(final String message, final Throwable cause,
                              final boolean enableSuppression,
                              final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
