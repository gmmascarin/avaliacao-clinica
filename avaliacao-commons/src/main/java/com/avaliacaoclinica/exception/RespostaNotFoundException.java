package com.avaliacaoclinica.exception;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 6:35 PM
 */
public class RespostaNotFoundException extends Exception {

    public RespostaNotFoundException() {
        super("Resposta não encontrada");
    }
}
