package com.avaliacaoclinica.exception;

public class PerguntaCantBeDeletedException extends Exception {
    public PerguntaCantBeDeletedException() {
    }

    public PerguntaCantBeDeletedException(final String message) {
        super(message);
    }
}
