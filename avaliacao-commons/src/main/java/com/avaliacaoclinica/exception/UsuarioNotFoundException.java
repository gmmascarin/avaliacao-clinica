package com.avaliacaoclinica.exception;

public class UsuarioNotFoundException extends Exception {

    public UsuarioNotFoundException() {
    }

    public UsuarioNotFoundException(final String message) {
        super(message);
    }
}
