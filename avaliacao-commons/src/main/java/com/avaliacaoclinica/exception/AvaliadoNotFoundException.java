package com.avaliacaoclinica.exception;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 8:48 PM
 */
public class AvaliadoNotFoundException extends Exception {

    public AvaliadoNotFoundException() {
        super("Avaliado Não encontradado");
    }
}
