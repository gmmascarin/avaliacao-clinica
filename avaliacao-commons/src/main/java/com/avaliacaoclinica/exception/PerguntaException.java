package com.avaliacaoclinica.exception;

public class PerguntaException extends Exception {

    public PerguntaException() {
    }

    public PerguntaException(final String message) {
        super(message);
    }
}
