package com.avaliacaoclinica.exception;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/29/17 9:41 PM
 */
public class AvaliadoJaCadastradoException extends Exception {

    public AvaliadoJaCadastradoException() {
        super("Avaliado já cadastrado");
    }

}
