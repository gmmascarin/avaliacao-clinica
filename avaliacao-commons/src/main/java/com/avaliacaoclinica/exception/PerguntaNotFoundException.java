package com.avaliacaoclinica.exception;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 9/29/17 6:32 PM
 */
public class PerguntaNotFoundException extends Exception {

    public PerguntaNotFoundException() {
        super("Pergunta não encontrada");
    }
}
