package com.avaliacaoclinica.vo;

import com.google.gson.Gson;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/31/17 7:10 AM
 */
@Data
@ToString
@NoArgsConstructor
public class GraficoVO {

    private String pergunta;
    private List<GraficoItemVO> item;

    public String getItemJson() {
        Gson gson = new Gson();
        return gson.toJson(this.item);
    }

    public List<GraficoItemVO> getItem() {
        return this.item;
    }
}
