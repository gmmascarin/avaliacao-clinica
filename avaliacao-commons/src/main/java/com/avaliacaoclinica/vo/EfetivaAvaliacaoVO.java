package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/29/17 8:39 PM
 */
@Data
@NoArgsConstructor
@ToString
public class EfetivaAvaliacaoVO {
    private AvaliadoVO avaliadoVO;

    private List<PerguntaRespostaVO> perguntasRespostas;

    @NotNull(message = "Informar o id do questionario")
    private Integer idQuestionario;
}
