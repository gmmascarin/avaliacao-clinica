package com.avaliacaoclinica.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ControleAcessoQuestionarioVO {

    @NotNull(message = "questionarioID deve ser informado")
    private Integer questionarioId;

    @NotNull
    @Size(min = 1)
    private List<UsuarioVO> usuariosAutorizados;

    private List<UsuarioVO> usuariosNaoAutorizados;
}
