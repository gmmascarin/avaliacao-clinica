package com.avaliacaoclinica.vo;

public enum TipoPerguntaVO {
    TEXTO("Simples"), MULTIPLA_ESCOLHA("Múltipla escolha"), EXPRESSAO(
            "Expressão");
    private String descricao;

    TipoPerguntaVO(final String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
