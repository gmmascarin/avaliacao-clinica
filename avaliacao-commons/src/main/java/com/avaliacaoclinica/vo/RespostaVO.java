package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/28/17 1:08 PM
 */
@Data
@NoArgsConstructor
@ToString
public class RespostaVO {

    private Long id;
    private String texto;
    private Integer posicao;

}
