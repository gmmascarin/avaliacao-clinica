package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/29/17 8:40 PM
 */
@Data
@NoArgsConstructor
@ToString
public class PerguntaRespostaVO {
    private Long idPergunta;
    private Long idResposta;
    private String textoResposta;
}
