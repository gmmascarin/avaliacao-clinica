package com.avaliacaoclinica.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ExportacaoVO {

    private String nome;
    private String pergunta;
    private String resposta;

    public Map<String, String> getPerguntaResposta(
            final String headerColunaPergunta,
            final String headerColunaResposta) {
        Map<String, String> perguntaResposta = new HashMap<String, String>();
        perguntaResposta.put(headerColunaPergunta, pergunta);
        perguntaResposta.put(headerColunaResposta, resposta);
        return perguntaResposta;
    }
}
