package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/29/17 6:05 PM
 */
@Data
@NoArgsConstructor
@ToString
public class AvaliacaoVO {
    private Long idAvaliacao;
    private String data;
    private Long idAvaliado;
    private String nomeAvaliado;
    private Integer idQuestionario;
    private String nomeQuestionario;

}
