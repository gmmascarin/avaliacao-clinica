package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/28/17 1:07 PM
 */
@Data
@NoArgsConstructor
@ToString
public class PerguntaVO {

    private Long id;
    private String texto;
    private TipoPerguntaVO tipo;
    private Integer posicao;
    private List<RespostaVO> respostas;
    private Integer questionarioId;

}
