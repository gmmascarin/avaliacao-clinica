package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class NovaPerguntaVO {
    private Integer idQuestionario;
    private Long idUsuario;
    private String tituloQuestionario;
    private String tipo;
    private String texto;
    private String expressao;
    private List<String> opcao;
    private List<Double> peso;

    public String getExpressao() {
        if (expressao == null) {
            return null;
        }
        return expressao.replaceAll("\\[", "\\(").replaceAll("\\]", "\\)");
    }
}
