package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/29/17 11:10 AM
 */
@Data
@NoArgsConstructor
@ToString
public class SimplesQuestionarioVO {

    private Integer id;
    private String titulo;
    private Integer countAvaliacoes;

}
