package com.avaliacaoclinica.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 10/19/17 6:06 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HomeVO {
    private Integer countQuestionarios;
    private Integer countAvaliacoes;
    private Integer countUsuarios;
    private Integer countPerguntas;
    private List<SimplesQuestionarioVO> listaQuestionario;
    private List<AvaliacaoVO> listaAvaliacao;
}
