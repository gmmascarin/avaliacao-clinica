package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 10/19/17 6:06 PM
 */
@Data
@ToString
public class HomeAppVO {

    private String tituloUltimoQuestionario;
    private Long totalPerguntasUltimoQuestionario;
    private Long totalAvaliacoesUltimoQuestionario;

}
