package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 11/6/17 12:49 PM
 */
@Data
@ToString
public class NovoUsuarioVO {
    private Long id;
    private String nome;
    private String login;
    private String senha;
    private String confirma;
    private String role;
}
