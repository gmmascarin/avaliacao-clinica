package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/29/17 9:13 PM
 */
@Data
@NoArgsConstructor
@ToString
public class AvaliadoVO {
    private Long id;

    @NotNull(message = "informar o nome do avaliado")
    private String nome;

    @NotNull(message = "Informar o cpf do avaliado")
    private String cpf;

    private String identificadorAtendimento;

    private Character sexoAbreviado;

    private String nascimento;

    private Integer grupoAtendimentoId;
}
