package com.avaliacaoclinica.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by gustavo on 01/04/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioVO {

    public UsuarioVO(final Long id) {
        this.id = id;
    }

    private Long id;

    @NotNull(message = "Favor informar o nome")
    @Size(min = 1, message = "Favor informar o nome")
    private String nome;

    @NotNull(message = "Favor informar o login")
    @Size(min = 1, message = "Favor informar o login")
    private String login;

    @NotNull(message = "Favor informar a senha")
    @Size(min = PASSWORD_MIN_CHARACTERES, message = "A senha deve ter pelo "
            + "menos 6 caracteres")
    private String senha;

    private RoleVO role;

    private static final int PASSWORD_MIN_CHARACTERES = 6;

    private static final int PRIME_NUMBER_TO_HASH = 31;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UsuarioVO usuarioVO = (UsuarioVO) o;

        if (id != null) {
            return id.equals(usuarioVO.id);
        }
        return usuarioVO.id == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        if (id != null) {
            result = PRIME_NUMBER_TO_HASH * result +  id.hashCode();
        } else {
            result = PRIME_NUMBER_TO_HASH * result;
        }
        return result;
    }

    @Override
    public String toString() {
        return "UsuarioVO{"
                + "nome='" + nome + '\''
                + ", login='" + login + '\''
                + '}';
    }
}
