package com.avaliacaoclinica.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 9/28/17 1:06 PM
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class QuestionarioVO extends SimplesQuestionarioVO {
    private List<PerguntaVO> perguntas;
}
