package com.avaliacaoclinica.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class GrupoAtendimentoVO {
    private Integer id;
    private String nome;
}
