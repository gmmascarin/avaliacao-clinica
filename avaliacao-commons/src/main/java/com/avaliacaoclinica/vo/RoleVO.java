package com.avaliacaoclinica.vo;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 11/6/17 11:08 AM
 */
public enum RoleVO {
    ROLE_ADMIN, ROLE_USER;
}
