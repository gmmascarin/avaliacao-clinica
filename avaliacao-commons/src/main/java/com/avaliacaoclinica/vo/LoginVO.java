package com.avaliacaoclinica.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by gustavo on 14/10/17.
 */
@Data
public class LoginVO {

    @NotNull(message = "Favor informar o login")
    @Size(min = 1, message = "Favor informar o login")
    private String usuario;

    @NotNull(message = "Favor informar a senha")
    @Size(min = PASSWORD_MAX_CHARACTERES, message = "A senha deve ter pelo "
            + "menos 6 caracteres")
    private String senha;

    private static final int PASSWORD_MAX_CHARACTERES = 6;

    @Override
    public String toString() {
        return "LoginVO{"
                + "usuario='" + usuario + '\''
                + '}';
    }
}
