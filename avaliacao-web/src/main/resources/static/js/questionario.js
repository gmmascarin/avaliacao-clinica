$(document).ready(function(){

    $('#select-questionario').change(function(){

        var url = "/avaliacao-web/questionarios/" + $(this).val() + "/relatorios/exportacao/";

        $('#select-avaliado').empty();
        if ($(this).val() > 0) {
            $('#select-avaliado').append('<option value="0"> Carregando...</option>');
            $('#select-avaliado').selectpicker('refresh');
        } else {
            $('#select-avaliado').append('<option value="0">-- Selecione --</option>');
            $('#select-avaliado').selectpicker('refresh');
            return;
        }

        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
               $('#select-avaliado').empty();
               $('#select-avaliado').append('<option value="0">-- Selecione --</option>');
               $.each(result, function(i, item) {
                   $('#select-avaliado').append('<option value="' + item.idAvaliado + '">' + item.nomeAvaliado + '</option>')
               });
               $('#select-avaliado').selectpicker('refresh');

             },
             error: function(e){
                console.log('deu erro: ', e);
             }
        });
    });

    $('a[data-method]').click(function (event) {
        event.preventDefault();
        if ($(this).data('confirm')) {
            var el = $(this);
            swal({
                title: $(this).data('confirm'),
                text: "Não será possível reverter essa ação!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Excluir",
                closeOnConfirm: false
            }, function () {
                funcaoExcluir(el);
            });
        } else {
            funcaoExcluir($(this));
        }
        return false;
    });

    function funcaoExcluir(el) {
        var form = $('<form></form>');
        form.attr('action', el.attr('href'));
        form.attr('method', 'post');

        var field = $('<input></input>');
        field.attr('type', 'hidden');
        field.attr('name', '_method');
        field.attr('value', el.data('method'));

        form.append(field);

        var field_origin = $('<input></input>');
        field_origin.attr('type', 'hidden');
        field_origin.attr('name', 'origin');
        field_origin.attr('value', el.data('origin'));

        form.append(field_origin);

        $(document.body).append(form);
        form.submit();
    }

    jQuery.extend(jQuery.validator.messages, {
        required: "Campo obrigatório.",
        minlength: jQuery.validator.format("Por favor, fornecer ao menos {0} caracteres."),
    });

    $('#form-exportar').validate({
        highlight: function (input) {
          $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
          $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
          $(element).parents('.form-group').append(error);
        }
    });

    $('#form-grafico').validate({
        highlight: function (input) {
          $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
          $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
          $(element).parents('.form-group').append(error);
        }
    });

    $('#select-questionario-grafico').change(function(){

        var url = "/avaliacao-web/questionarios/" + $(this).val() + "/relatorios/grafico/perguntas";

        $('#select-pergunta').empty();
        if ($(this).val() > 0) {
            $('#select-pergunta').append('<option value="0"> Carregando...</option>');
            $('#select-pergunta').selectpicker('refresh');
        } else {
            $('#select-pergunta').append('<option value="0">-- Selecione --</option>');
            $('#select-pergunta').selectpicker('refresh');
            return;
        }

        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
               $('#select-pergunta').empty();
                $('#select-pergunta').append('<option value="0">-- Selecione --</option>');
               $.each(result, function(i, item) {
                   $('#select-pergunta').append('<option value="' + item.id + '">' + item.texto + '</option>')
               });
               $('#select-pergunta').selectpicker('refresh');

             },
             error: function(e){
                console.log('deu erro: ', e);
             }
        });
    });

    $('#perfil').click(function() {
        var form = $('<form></form>');
        form.attr('action', el.attr('href'));
        form.attr('method', 'get');

        var field = $('<input></input>');
        field.attr('type', 'hidden');
        field.attr('name', '_method');
        field.attr('value', 'get');

        form.append(field);
        $(document.body).append(form);
        form.submit();
    });

    $('.datatable-responsive').DataTable({
            responsive: true
    });
});