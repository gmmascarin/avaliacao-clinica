$(document).ready(function(){

//    Responsável por controlar as opções de resposta nas perguntas de múltipla escolha
    $('#addOption').click(function(){

        var qtd = $('.options input[name^="opcao"]').length;

        var $div = $("<div></div>", {"class": "input-group"}).appendTo(".options");
        var $span = $("<span></span>", {"class": "input-group-addon"}).appendTo($div);
        var $input = $("<input></input>", {"class": "with-gap", "type": "radio", "disabled":"disabled"}).appendTo($span);
        var $label = $("<label></label>", {"class": "lbl"}).appendTo($span);

        var $div10 = $("<div></div>", {"class": "col-sm-10"}).appendTo($div);
        var $divLine = $("<div></div>", {"class": "form-line"}).appendTo($div10);
        var $inputText = $("<input></input>", {"class":"form-control", "name":"opcao[" + qtd + "]", "type": "text", "placeholder":"Digite a opção"}).appendTo($divLine);

        var $div2 = $("<div></div>", {"class": "col-sm-2"}).appendTo($div);
        var $divLinePeso = $("<div></div>", {"class": "form-line"}).appendTo($div2);
        var $inputTextPeso = $("<input></input>", {"class":"form-control", "name":"peso[" + qtd + "]", "type": "text", "placeholder":"Digite o peso"}).appendTo($divLinePeso);

        $inputTextPeso.on('change', function(event){func_replace_comma($(this))})

        $.AdminBSB.input.activate();

         $('.form-control').each(function () {
            if ($(this).val() !== '') {
                $(this).parents('.form-line').removeClass('focused');
            }
        });
    });

    $('#form-nova-pergunta').validate({
        highlight: function (input) {
          $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
          $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
          $(element).parents('.form-group').append(error);
        }
    });

//    Submita o form ao clicar no salva pergunta
    $('#btn-salvar-pergunta').click(function() {
        $('form#form-nova-pergunta').submit();

    });

    var func_replace_comma = function(el) {
         var peso = el.val().replace(',', '.');
         var valid = !isNaN(parseFloat(peso)) && isFinite(peso);

         if (valid == true) {
             el.val(peso);
         } else {
             el.val('');
         }
     }

    //Validamos se o peso está digitado corretamente
    $('.options input[name^="peso"]').change(function(event){func_replace_comma($(this))});

//    Responsável pela exibição das opções nas questoes de multipla escolha
    $( "#tipo" ).change(function() {
        if($(this).val() == 'MULTIPLA_ESCOLHA'){
            $( ".div-multipla-escolha" ).show();
        } else {
            $( ".div-multipla-escolha" ).hide();
        }

        if($(this).val() == 'EXPRESSAO'){
            $( ".div-expressao" ).show();
        } else {
            $( ".div-expressao" ).hide();
        }
    });

//    Quando o usuário clicar em Nova Pergunta na tela de questionarios, deverá exibir o modal com o titulo do questionario
//    e adicionar as validações em caso de erro no preenchimento
    $('#btn-nova-pergunta').click(function(event) {
        var titulo = $('#titulo').val();
        $("#tituloQuestionario").val( titulo );
        $(".small-titulo").text('Questionário: ' + titulo);

        var idQuestionario = $('#id').val();
        $("#idQuestionario").val( idQuestionario );

        $( "#tipo" ).val('');
        $( "#tipo" ).change();
        $('#tipo').selectpicker('refresh');

        var formulario = $('#formularioQuestionario');
        formulario.validate({
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

        if (!formulario.valid()){
            return false;
        }
    });


});