$(function () {
    $('.grafico_donut_pergunta').each(function(){
        if ($(this).data('info').length > 0) {
            Morris.Donut({
                      element: $(this),
                      data: $(this).data('info'),
                      colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)'],
                          formatter: function (y) {
                              return y + '%'
                              }
                    });
        }
    });

    $('.grafico_bar_pergunta').each(function(){
           if ($(this).data('info').length > 0) {
                Morris.Bar({
                  element: $(this),
                  data: $(this).data('info'),
                  xkey: 'label',
                  ykeys: ['value'],
                  labels: ['%']
                });
            }
        });


});