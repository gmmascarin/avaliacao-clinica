package com.avaliacaoclinica.security;

import org.springframework.security.web.context
        .AbstractSecurityWebApplicationInitializer;

public class SpringWebSecurityInitializer extends
        AbstractSecurityWebApplicationInitializer {
}
