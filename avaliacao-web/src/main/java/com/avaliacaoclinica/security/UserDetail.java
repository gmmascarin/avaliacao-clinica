package com.avaliacaoclinica.security;

import com.avaliacaoclinica.vo.UsuarioVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserDetail extends User {

    public UserDetail(final UsuarioVO usuarioVO, final
    List<SimpleGrantedAuthority> authorities) {
        super(usuarioVO.getLogin(), usuarioVO.getSenha(), authorities);
        this.usuarioVO = usuarioVO;
    }

    private UsuarioVO usuarioVO;
}
