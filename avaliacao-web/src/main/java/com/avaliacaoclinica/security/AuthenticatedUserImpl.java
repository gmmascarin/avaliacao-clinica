package com.avaliacaoclinica.security;

import com.avaliacaoclinica.vo.UsuarioVO;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 11/6/17 4:18 PM
 */
@Component
public class AuthenticatedUserImpl implements AuthenticatedUser {

    @Override
    public UsuarioVO getUserAuthenticated() {
        return ((UserDetail) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal()).getUsuarioVO();
    }
}
