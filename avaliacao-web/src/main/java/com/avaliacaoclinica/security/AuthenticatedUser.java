package com.avaliacaoclinica.security;

import com.avaliacaoclinica.vo.UsuarioVO;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 11/6/17 4:18 PM
 */
public interface AuthenticatedUser {

    UsuarioVO getUserAuthenticated();

}
