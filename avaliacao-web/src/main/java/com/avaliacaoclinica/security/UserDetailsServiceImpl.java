package com.avaliacaoclinica.security;

import com.avaliacaoclinica.client.AvaliacaoApiClient;
import com.avaliacaoclinica.vo.UsuarioVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AvaliacaoApiClient avaliacaoApiClient;

    @Override
    public UserDetails loadUserByUsername(final String username) throws
            UsernameNotFoundException {
        UsuarioVO usuarioVO = avaliacaoApiClient.buscarUsuario(username);
        if (usuarioVO == null || usuarioVO.getId() == null) {
            throw new UsernameNotFoundException("Usuário não encontrado");
        }
        return new UserDetail(usuarioVO, Arrays.asList(new
                SimpleGrantedAuthority(usuarioVO.getRole().name())));
    }
}
