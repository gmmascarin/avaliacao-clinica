package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.client.AvaliacaoApiClient;
import com.avaliacaoclinica.security.AuthenticatedUser;
import com.avaliacaoclinica.vo.HomeVO;
import com.avaliacaoclinica.vo.UsuarioVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Created by gustavo on 17/12/16.
 */
@Controller
@Slf4j
public class HomeController {

    @Autowired
    private AvaliacaoApiClient avaliacaoApiClient;

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @RequestMapping(method = RequestMethod.GET, path = {"/", "home"})
    public String home(final Model model, final HttpServletRequest request,
                       final Principal
            principal) {
        UsuarioVO usuarioVO = authenticatedUser.getUserAuthenticated();
        log.info("M=home, " + usuarioVO);

        final HomeVO home = avaliacaoApiClient.home(usuarioVO.getId());
        model.addAttribute("homeVO", home);
        return "index";
    }

    @GetMapping(value = "/login")
    public String login() {
        log.info("M=login");
        return "login";
    }
}
