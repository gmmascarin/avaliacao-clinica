package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.client.AvaliacaoApiClient;
import com.avaliacaoclinica.security.AuthenticatedUser;
import com.avaliacaoclinica.vo.AvaliacaoVO;
import com.avaliacaoclinica.vo.ExportacaoVO;
import com.avaliacaoclinica.vo.GraficoVO;
import com.avaliacaoclinica.vo.PerguntaVO;
import com.avaliacaoclinica.vo.QuestionarioVO;
import com.avaliacaoclinica.vo.SimplesQuestionarioVO;
import com.avaliacaoclinica.vo.UsuarioVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@Slf4j
public class RelatoriosController {

    public static final String LABEL_NOME = "Nome";

    @Autowired
    private AvaliacaoApiClient avaliacaoApiClient;

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @RequestMapping(value = "/relatorios/graficosQuestionario", method =
            RequestMethod.GET)
    public String buscarGraficosDoQuestionario(
            @RequestParam(value = "questionarioId", required = false) final
            Integer questionarioId,
            @RequestParam(value = "perguntaId", required = false) final
            Long perguntaId,
            final Model model, final HttpServletRequest request) {
        log.info("M=buscarGraficosDoQuestionario, questionarioId="
                + questionarioId);
        UsuarioVO usuarioVO = authenticatedUser.getUserAuthenticated();
        if (questionarioId != null) {
            List<GraficoVO> graficosInfo = avaliacaoApiClient.getGraficosInfo(
                    questionarioId, perguntaId);
            log.info(
                    "M=buscarGraficosDoQuestionario, graficosInfo={}",
                    graficosInfo);

            model.addAttribute("graficosInfo", graficosInfo);

        } else {
            model.addAttribute("questionario", new QuestionarioVO());
        }

        List<SimplesQuestionarioVO> todosQuestionarios = avaliacaoApiClient
                .getQuestionariosByUserId(usuarioVO.getId());
        model.addAttribute("todosQuestionarios", todosQuestionarios);

        return "graficosQuestionario";
    }

    @RequestMapping(value = "/relatorios/exportacao", method = RequestMethod
            .GET, produces = "text/csv")
    public String relatorioExportacao(
            final Model model, final HttpServletRequest
            request, final HttpServletResponse response) {
        log.info("M=relatorioExportacao");
        UsuarioVO usuarioVO = authenticatedUser.getUserAuthenticated();

        List<SimplesQuestionarioVO> questionarios = avaliacaoApiClient
                .getQuestionariosByUserId(usuarioVO.getId());
        if (questionarios != null && !questionarios.isEmpty()) {
            model.addAttribute("questionarios", questionarios);

            List<AvaliacaoVO> avaliacoes = avaliacaoApiClient
                    .getAvaliacoesByUserId(usuarioVO.getId());
            if (avaliacoes != null && !avaliacoes.isEmpty()) {
                model.addAttribute("avaliacoes", avaliacoes);
            }
        }
        return "relatorioExportacao";
    }

    @RequestMapping(value = "questionarios/{questionarioId}/relatorios"
            + "/exportacao/", method = RequestMethod.GET)
    @ResponseBody
    public List<AvaliacaoVO> ajaxBuscarAvaliados(
            @PathVariable
                    ("questionarioId") final Integer questionarioId, final
    Model model) {
        log.info("M=ajaxBuscarAvaliados, questionarioId=" + questionarioId);
        List<AvaliacaoVO> avaliacoes = avaliacaoApiClient
                .getAvaliacoesByQuestionarioId(questionarioId);
        if (avaliacoes != null && !avaliacoes.isEmpty()) {
            model.addAttribute("avaliacoes", avaliacoes);
        }
        return avaliacoes;
    }

    @RequestMapping(value = "questionarios/{questionarioId}/relatorios"
            + "/grafico/perguntas", method = RequestMethod.GET)
    @ResponseBody
    public List<PerguntaVO> ajaxBuscarPerguntas(
            @PathVariable
                    ("questionarioId") final Integer questionarioId, final
    Model model) {
        log.info("M=ajaxBuscarPerguntas, questionarioId=" + questionarioId);
        List<PerguntaVO> perguntas = avaliacaoApiClient
                .buscarPerguntasDoQuestionario(questionarioId);
        if (perguntas != null && !perguntas.isEmpty()) {
            model.addAttribute("perguntas", perguntas);
        }
        return perguntas;
    }

    @RequestMapping(value = "/relatorios/exportar", method = RequestMethod
            .GET, produces = "text/csv")
    public String exportarRelatorio(
            final Model model, final HttpServletRequest request, final
    HttpServletResponse
            response,
            @RequestParam(value = "questionarioId", required = false) final
            Integer
                    questionarioId,
            @RequestParam(value = "avaliadoId", required = false) final Long
                    avaliadoId,
            final RedirectAttributes redirectAttrs) {
        log.info("M=exportarRelatorio, questionarioId=" + questionarioId + ","
                + " avaliadoId=" + avaliadoId);
        if (questionarioId != null) {
            try {
                exportarRelatorio(questionarioId, avaliadoId, response);
            } catch (IOException e) {
                redirectAttrs.addFlashAttribute("errorMessages", "Não foi "
                        + "possível emitir o relatório");
                log.error("M=exportarRelatorio", e);
                return "redirect:/relatorios/exportacao";
            }
        } else {
            redirectAttrs.addFlashAttribute("errorMessages", "Favor "
                    + "selecionar o questionário");
            log.error("M=exportarRelatorio, M=nao informado todos os campos");
            return "redirect:/relatorios/exportacao";
        }
        return null;
    }

    private void exportarRelatorio(
            final Integer questionarioId, final Long avaliadoId,
            final HttpServletResponse response) throws
            IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; "
                + "filename=\"avaliacao.csv\"");
        response.setHeader("pragma", "public");
        response.setHeader("Cache-control", "must-revalidate");

        List<ExportacaoVO> exportacaoVOList = avaliacaoApiClient.exportar(
                questionarioId, avaliadoId);
        if (exportacaoVOList == null || exportacaoVOList.isEmpty()) {
            return;
        }

        String[] header = getHeader(exportacaoVOList);

        ICsvMapWriter mapWriter = new CsvMapWriter(
                response.getWriter(),
                CsvPreference.STANDARD_PREFERENCE);

        mapWriter.writeHeader(header);

        for (Map<String, Object> linha : getLinhasRelatorio(
                exportacaoVOList,
                header)) {
            mapWriter.write(linha, header);
        }

        mapWriter.close();
    }

    public Set<Map<String, Object>> getLinhasRelatorio(
            final List<ExportacaoVO>
                    exportacaoVOList, final String[] header) {

        String lastName = exportacaoVOList.get(0).getNome();
        Map<String, Object> mapLinha = new HashMap<>();
        mapLinha.put(LABEL_NOME, exportacaoVOList.get(0).getNome());
        Set<Map<String, Object>> listaLinhas = new HashSet<>();
        listaLinhas.add(mapLinha);

        int contHeader = 0;
        for (ExportacaoVO exportacaoVO : exportacaoVOList) {
            contHeader++;
            if (!exportacaoVO.getNome().equals(lastName)) {
                contHeader = 1; //Nome não é incluído
                mapLinha = new HashMap<>();
                listaLinhas.add(mapLinha);
                mapLinha.put(LABEL_NOME, exportacaoVO.getNome());
            }

            if (header[contHeader].equals(exportacaoVO.getPergunta())) {
                mapLinha.put(header[contHeader], exportacaoVO.getResposta());
            } else {
                mapLinha.put(header[contHeader], exportacaoVO.getResposta());
            }

            lastName = exportacaoVO.getNome();
        }
        return listaLinhas;
    }

    public String[] getHeader(final List<ExportacaoVO> exportacaoVOList) {
        Set<String> headers = new LinkedHashSet<>();
        for (ExportacaoVO exportacaoVO : exportacaoVOList) {
            headers.add(exportacaoVO.getPergunta());
        }

        String[] headersArray = new String[exportacaoVOList.size() + 1];
        headersArray[0] = LABEL_NOME;
        int i = 1;
        for (String header : headers) {
            headersArray[i++] = header;
        }
        return headersArray;
    }

}
