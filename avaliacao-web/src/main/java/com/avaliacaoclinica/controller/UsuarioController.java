package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.client.AvaliacaoApiClient;
import com.avaliacaoclinica.security.UserDetail;
import com.avaliacaoclinica.vo.NovoUsuarioVO;
import com.avaliacaoclinica.vo.RoleVO;
import com.avaliacaoclinica.vo.UsuarioVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/25/17 1:10 PM
 */
@Controller
@Slf4j
public class UsuarioController {

    @Autowired
    private AvaliacaoApiClient avaliacaoApiClient;

    @RequestMapping(method = RequestMethod.GET, value = "usuarios")
    public String usuarios(final Model model) {
        log.info("M=usuarios");
        final List<UsuarioVO> usuarios = avaliacaoApiClient.buscarUsuarios();
        model.addAttribute("vo", usuarios);
        return "usuarios";
    }

    @RequestMapping(value = "/usuarios/editar", method = RequestMethod.GET)
    public String editarUsuario(
            final Model model, final HttpServletRequest request,
            @RequestParam(value = "usuarioId", required = false) final Long
                    usuarioId) {
        log.info("M=editarUsuario, usuarioId=" + usuarioId);

        if (usuarioId != null) {
            //Request veio do editar
            UsuarioVO usuarioVO = avaliacaoApiClient.buscarUsuario(usuarioId);
            model.addAttribute("vo", usuarioVO);
        }
        return "editarUsuario";
    }

    @RequestMapping(value = "/usuarios/profile")
    public String profile(final Model model) {
        final Long usuarioId = (
                (UserDetail) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal())
                .getUsuarioVO().getId();
        UsuarioVO usuarioVO = avaliacaoApiClient.buscarUsuario(usuarioId);
        model.addAttribute("vo", usuarioVO);
        return "editarUsuario";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/usuarios/form")
    public String usuarioForm(final Model model) {
        log.info("M=usuarioForm");
        return "novoUsuario";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/usuarios")
    public String cadastrarUsuario(
            final NovoUsuarioVO novoUsuarioVO, final
    RedirectAttributes redirectAttrs) {
        log.info("M=cadastrarUsuario, novoUsuarioVO=" + novoUsuarioVO);
        final UsuarioVO usuarioVO = avaliacaoApiClient.cadastrarUsuario(
                novoUsuarioVO);
        if (usuarioVO == null || usuarioVO.getId() == null) {
            redirectAttrs.addFlashAttribute("errorMessage", "Não foi possível"
                    + " cadastrar o Usuário");
        } else {
            redirectAttrs.addFlashAttribute("successMessage", "Usuário "
                    + "cadastrado com sucesso");
        }
        return "redirect:login";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/usuarios")
    public String atualizarUsuario(
            final NovoUsuarioVO novoUsuarioVO, final
    RedirectAttributes redirectAttrs) {
        log.info("M=atualizarUsuario, novoUsuarioVO=" + novoUsuarioVO);
        final UsuarioVO usuarioVO = avaliacaoApiClient.atualizarUsuario(
                novoUsuarioVO);
        if (usuarioVO == null || usuarioVO.getId() == null) {
            redirectAttrs.addFlashAttribute("errorMessage", "Não foi possível"
                    + " atualizar o Usuário");
        } else {
            redirectAttrs.addFlashAttribute("successMessage", "Usuário "
                    + "atualizado com sucesso");
        }

        final RoleVO role = (
                (UserDetail) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal())
                .getUsuarioVO().getRole();
        if (RoleVO.ROLE_ADMIN.equals(role)) {
            return "redirect:usuarios";
        }

        return "redirect:home";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/usuarios/{id}")
    public String excluirUsuario(
            @PathVariable(value = "id", required = true) final Long id, final
    RedirectAttributes
            redirectAttrs) {
        log.info("M=excluirUsuario, id=" + id);
        try {
            final Boolean success = avaliacaoApiClient.excluirUsuario(id);
            if (success) {
                redirectAttrs.addFlashAttribute("successMessage", "Usuário "
                        + "excluído com sucesso");
            } else {
                redirectAttrs.addFlashAttribute("errorMessage", "Não foi "
                        + "possível excluir o usuário");
            }
        } catch (Exception e) {
            log.error("M=excluirUsuario", e);
            redirectAttrs.addFlashAttribute("errorMessage", "Erro ao excluir "
                    + "o usuário");
        }
        return "redirect:/usuarios";
    }

}
