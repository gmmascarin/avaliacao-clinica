package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.client.AvaliacaoApiClient;
import com.avaliacaoclinica.security.AuthenticatedUser;
import com.avaliacaoclinica.vo.AvaliacaoVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/25/17 12:57 PM
 */
@Controller
@Slf4j
public class AvaliacaoController {

    @Autowired
    private AvaliacaoApiClient avaliacaoApiClient;

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @RequestMapping(method = RequestMethod.GET, value = "avaliacoes")
    public String avaliacoes(final Model model) {
        log.info("M=avaliacoes");
        final List<AvaliacaoVO> listaAvaliacao = avaliacaoApiClient.
                getAvaliacoesByUserId(
                        authenticatedUser.getUserAuthenticated().getId()
                );
        model.addAttribute("listaAvaliacao", listaAvaliacao);
        return "avaliacoes";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/avaliacoes/{id}")
    public String excluirAvaliacao(@PathVariable(value = "id", required =
            true) final Long avaliacaoId,
                                   @RequestParam(value = "origin", required =
                                           false) final String origem,
                                   final RedirectAttributes redirectAttrs) {
        log.info("M=excluirAvaliacao, avaliacaoId=" + avaliacaoId + ", "
                + "origem=" + origem);
        try {
            final Boolean success = avaliacaoApiClient.excluirAvaliacao(
                    avaliacaoId);
            if (success) {
                redirectAttrs.addFlashAttribute("successMessage", "Avaliação "
                        + "excluída com sucesso");
            } else {
                redirectAttrs.addFlashAttribute("errorMessage", "Não foi "
                        + "possível excluir a avaliação");
            }
        } catch (Exception e) {
            log.error("M=excluirAvaliacao", e);
            redirectAttrs.addFlashAttribute("errorMessage", "Erro ao excluir "
                    + "a avaliação");
        }

        if (StringUtils.isNotEmpty(origem) && origem.equalsIgnoreCase("home")) {
            return "redirect:/home";
        } else {
            return "redirect:/avaliacoes";
        }
    }

}
