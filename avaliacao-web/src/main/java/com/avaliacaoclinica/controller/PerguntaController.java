package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.client.AvaliacaoApiClient;
import com.avaliacaoclinica.exception.AvaliacaoException;
import com.avaliacaoclinica.vo.PerguntaVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Slf4j
public class PerguntaController {

    @Autowired
    private AvaliacaoApiClient avaliacaoApiClient;

    @RequestMapping(method = RequestMethod.DELETE, value = "/perguntas/{id}")
    public String excluirPergunta(
            @PathVariable(value = "id", required =
                    true) final Long perguntaId,
            final RedirectAttributes redirectAttrs) {
        log.info("M=excluirPergunta, perguntaId=" + perguntaId);
        PerguntaVO perguntaVO = null;
        try {
            perguntaVO = avaliacaoApiClient.buscarPergunta(perguntaId);
            if (perguntaVO == null) {
                log.error("M=excluirPergunta, msg=pergunta nao encontrada");
                redirectAttrs.addFlashAttribute("errorMessage", "Não foi "
                        + "possível excluir a Pergunta");
                return "redirect:/questionarios/perguntas";
            }
            final Boolean success = avaliacaoApiClient.excluirPergunta(
                    perguntaId);
            if (success) {
                redirectAttrs.addFlashAttribute("successMessage", "Pergunta "
                        + "excluída com sucesso");
            } else {
                redirectAttrs.addFlashAttribute("errorMessage", "Não foi "
                        + "possível excluir a Pergunta");
            }
        } catch (AvaliacaoException e) {
            log.error("M=excluirPergunta", e);
            if (e.getMessage() != null) {
                redirectAttrs.addFlashAttribute(
                        "errorMessage",
                        e.getMessage());
            } else {
                redirectAttrs.addFlashAttribute("errorMessage", "Erro ao "
                        + "excluir o Questionário");
            }

        } catch (Exception e) {
            log.error("M=excluirPergunta", e);
            redirectAttrs.addFlashAttribute("errorMessage", "Erro ao excluir "
                    + "o Questionário");
        }
        return "redirect:/questionarios/perguntas?questionarioId="
                + perguntaVO.getQuestionarioId();
    }
}
