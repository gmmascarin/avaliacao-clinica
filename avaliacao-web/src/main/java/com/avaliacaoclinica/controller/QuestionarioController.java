package com.avaliacaoclinica.controller;

import com.avaliacaoclinica.client.AvaliacaoApiClient;
import com.avaliacaoclinica.exception.AvaliacaoException;
import com.avaliacaoclinica.security.AuthenticatedUser;
import com.avaliacaoclinica.vo.ControleAcessoQuestionarioVO;
import com.avaliacaoclinica.vo.NovaPerguntaVO;
import com.avaliacaoclinica.vo.QuestionarioVO;
import com.avaliacaoclinica.vo.SimplesQuestionarioVO;
import com.avaliacaoclinica.vo.UsuarioVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class QuestionarioController {

    @Autowired
    private AvaliacaoApiClient avaliacaoApiClient;

    @Autowired
    private AuthenticatedUser authenticatedUser;

    @RequestMapping(method = RequestMethod.GET, value = "questionarios")
    public String questionarios(final Model model) {
        log.info("M=questionarios");
        final UsuarioVO userAuthenticated = authenticatedUser
                .getUserAuthenticated();
        final List<SimplesQuestionarioVO> listaQuestionario = avaliacaoApiClient
                .getQuestionariosByUserId(userAuthenticated.getId());
        model.addAttribute("listaQuestionario", listaQuestionario);
        return "questionarios";
    }

    @RequestMapping(value = "/questionarios", method = RequestMethod.POST)
    public String novaPergunta(
            final Model model, @ModelAttribute final
    NovaPerguntaVO novaPerguntaVO,
            final RedirectAttributes redirectAttrs) {
        log.info("M=novaPergunta, novaPerguntaVO=" + novaPerguntaVO);
        Integer questionarioId = novaPerguntaVO.getIdQuestionario();
        UsuarioVO usuarioVO = authenticatedUser.getUserAuthenticated();
        List<String> errors = validate(novaPerguntaVO);
        novaPerguntaVO.setIdUsuario(usuarioVO.getId());
        if (!errors.isEmpty()) {
            QuestionarioVO questionarioVO = new QuestionarioVO();
            questionarioVO.setId(novaPerguntaVO.getIdQuestionario());
            questionarioVO.setTitulo(novaPerguntaVO.getTituloQuestionario());
            redirectAttrs.addFlashAttribute("vo", questionarioVO);
            redirectAttrs.addFlashAttribute("errorMessages", errors);
        } else {
            QuestionarioVO questionarioVO = null;
            try {
                questionarioVO = avaliacaoApiClient.salvarQuestionario(
                        novaPerguntaVO);
                if (questionarioVO.getId() != null) {
                    questionarioId = questionarioVO.getId();
                }
                redirectAttrs.addFlashAttribute("vo", questionarioVO);
                redirectAttrs.addFlashAttribute("successMessage", "Pergunta "
                        + "cadastrada com sucesso.");
            } catch (AvaliacaoException e) {
                log.error("M=novaPergunta", e);
                redirectAttrs.addFlashAttribute("errorMessage", e.getMessage());
            }
        }
        return "redirect:/questionarios/perguntas?questionarioId="
                + questionarioId;
    }

    @RequestMapping("/questionarios/perguntas")
    public String perguntasDoQuestionario(
            final Model model,
            @ModelAttribute(value = "vo") final
            QuestionarioVO questionarioVO,
            @RequestParam(value =
                    "questionarioId", required
                    = false) final Integer
                    questionarioId) {
        log.info("M=perguntasDoQuestionario, questionarioVO="
                + questionarioVO + ", questionarioId=" + questionarioId);

        QuestionarioVO questionarioVOFromBD = new QuestionarioVO();

        if (questionarioId != null) {
            // Request veio do editar
            questionarioVOFromBD = avaliacaoApiClient.getQuestionarioPorId(
                    questionarioId);
        }

        if (questionarioVO != null) {
            model.addAttribute("vo", questionarioVOFromBD);
        } else {
            model.addAttribute("vo", new QuestionarioVO());
        }
        return "novoQuestionario";
    }

    private List<String> validate(final NovaPerguntaVO novaPerguntaVO) {
        List<String> errors = new ArrayList<>();
        if (novaPerguntaVO == null) {
            errors.add("Dados da pergunta não informados");
            return errors;
        }

        if (novaPerguntaVO.getIdQuestionario() == null && StringUtils.isBlank(
                novaPerguntaVO.getTituloQuestionario())) {
            errors.add("Questionário incorreto");
        }
        if (novaPerguntaVO.getTipo().equals("MULTIPLA_ESCOLHA")
                && (
                novaPerguntaVO.getOpcao() == null
                        || novaPerguntaVO.getOpcao().isEmpty())) {
            errors.add("Necessário preencher pelo menos uma opção");
        }
        if (novaPerguntaVO.getTipo().equals("EXPRESSAO")) {
            if (StringUtils.isBlank(novaPerguntaVO.getExpressao())) {
                errors.add("Informar a Expressão");
            } else {
                if (!novaPerguntaVO.getExpressao().trim()
                        .matches("^(p|P|\\"
                                + "(|\\().*")) {
                    errors.add("Expressão incorreta");
                }
            }
        }
        if (StringUtils.isBlank(novaPerguntaVO.getTexto())) {
            errors.add("Informar o texto da pergunta");
        }
        return errors;
    }

    @RequestMapping(method = RequestMethod.DELETE, value =
            "/questionarios/{id}")
    public String excluirQuestionario(
            @PathVariable(value = "id", required =
                    true) final Integer questionarioId,
            @RequestParam(value = "origin",
                    required = false) final String
                    origem,
            final RedirectAttributes redirectAttrs) {
        log.info("M=excluirQuestionario, questionarioId=" + questionarioId
                + ", origem=" + origem);
        try {
            final Boolean success = avaliacaoApiClient.excluirQuestionario(
                    questionarioId);
            if (success) {
                redirectAttrs.addFlashAttribute(
                        "successMessage",
                        "Questionário excluído com sucesso");
            } else {
                redirectAttrs.addFlashAttribute("errorMessage", "Não foi "
                        + "possível excluir o Questionário");
            }
        } catch (AvaliacaoException e) {
            log.error("M=excluirQuestionario", e);
            if (e.getMessage() != null) {
                redirectAttrs.addFlashAttribute(
                        "errorMessage", e.getMessage());
            } else {
                redirectAttrs.addFlashAttribute(
                        "errorMessage", "Erro ao excluir o Questionário");
            }
        } catch (Exception e) {
            log.error("M=excluirQuestionario", e);
            redirectAttrs.addFlashAttribute("errorMessage", "Erro ao excluir "
                    + "o Questionário");
        }

        if (StringUtils.isNotEmpty(origem) && origem.equalsIgnoreCase("home")) {
            return "redirect:/home";
        } else {
            return "redirect:/questionarios";
        }
    }

    @RequestMapping(value = "/questionarios/controleAcesso", method =
            {RequestMethod.GET, RequestMethod.POST})
    public String controleAcesso(
            final Model model, @RequestParam(value =
            "questionarioId", required = true) final Integer questionarioId,
            @RequestParam(value = "usuariosAutorizados",
                    required = false) final List<Long>
                    usuariosAutorizados) {
        log.info("M=controleAcesso, questionarioId=" + questionarioId + ", "
                + "autorizados=" + usuariosAutorizados);
        final UsuarioVO userAuthenticated = authenticatedUser
                .getUserAuthenticated();
        ControleAcessoQuestionarioVO controleAcessoQuestionarioVO = null;

        if (usuariosAutorizados != null && !usuariosAutorizados.isEmpty()) {

            if (!usuariosAutorizados.contains(userAuthenticated.getId())) {
                model.addAttribute("errorMessage", "Não é possível remover "
                        + "acesso do seu próprio usuário.");
                controleAcessoQuestionarioVO = avaliacaoApiClient
                        .getControleAcessoQuestionario(questionarioId);
            } else {
                List<UsuarioVO> usuariosVOSAutorizados = usuariosAutorizados
                        .stream().map(UsuarioVO::new).collect(Collectors
                                .toList());
                controleAcessoQuestionarioVO = avaliacaoApiClient
                        .atualizarControleAcessoQuestionario(new
                                ControleAcessoQuestionarioVO(questionarioId,
                                usuariosVOSAutorizados, null));
                model.addAttribute("successMessage", "Permissões alteradas "
                        + "com sucesso.");
            }
        } else {
            controleAcessoQuestionarioVO = avaliacaoApiClient
                    .getControleAcessoQuestionario(questionarioId);
        }
        model.addAttribute("controleAcesso", controleAcessoQuestionarioVO);
        return "controleAcessoQuestionario";
    }
}
