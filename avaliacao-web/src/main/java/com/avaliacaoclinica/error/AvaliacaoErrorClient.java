package com.avaliacaoclinica.error;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 10/26/17 3:32 PM
 */
public class AvaliacaoErrorClient extends Exception {
    private String message;

    @Override
    public String getMessage() {
        return message;
    }

}
