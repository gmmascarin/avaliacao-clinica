package com.avaliacaoclinica.error;

import com.avaliacaoclinica.exception.AvaliacaoException;
import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;

import java.io.IOException;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/26/17 3:30 PM
 */
public class AvaliacaoErrorDecoder implements ErrorDecoder {

    private final Decoder decoder;
    private final ErrorDecoder defaultDecoder = new ErrorDecoder.Default();

    public AvaliacaoErrorDecoder(final Decoder decoder) {
        this.decoder = decoder;
    }

    @Override
    public Exception decode(final String methodKey, final Response response) {
        try {
            final Object decode = decoder.decode(response,
                    AvaliacaoErrorClient.class);
            return new AvaliacaoException(((Exception) decode).getMessage());
        } catch (IOException e) {
            return defaultDecoder.decode(methodKey, response);
        }
    }
}
