package com.avaliacaoclinica.client;

import com.avaliacaoclinica.exception.AvaliacaoException;
import com.avaliacaoclinica.vo.AvaliacaoVO;
import com.avaliacaoclinica.vo.ControleAcessoQuestionarioVO;
import com.avaliacaoclinica.vo.ExportacaoVO;
import com.avaliacaoclinica.vo.GraficoVO;
import com.avaliacaoclinica.vo.HomeVO;
import com.avaliacaoclinica.vo.NovaPerguntaVO;
import com.avaliacaoclinica.vo.NovoUsuarioVO;
import com.avaliacaoclinica.vo.PerguntaVO;
import com.avaliacaoclinica.vo.QuestionarioVO;
import com.avaliacaoclinica.vo.SimplesQuestionarioVO;
import com.avaliacaoclinica.vo.UsuarioVO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.List;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 * $Id: $
 * @since 10/19/17 6:57 PM
 */
public interface AvaliacaoApiClient {

    @RequestLine("GET /home/{id}")
    HomeVO home(@Param("id") Long id);

    @RequestLine("GET /usuarios/{id}/questionarios")
    List<SimplesQuestionarioVO> getQuestionariosByUserId(@Param("id") Long id);

    @RequestLine("GET /usuarios/{id}/avaliacoes")
    List<AvaliacaoVO> getAvaliacoesByUserId(@Param("id") Long id);

    @RequestLine("GET /questionarios/{id}")
    QuestionarioVO getQuestionarioPorId(@Param("id") Integer id);

    @RequestLine("GET /questionarios/{id}/avaliacoes")
    List<AvaliacaoVO> getAvaliacoesByQuestionarioId(@Param("id") Integer id);

    @RequestLine("POST /questionarios")
    @Headers("Content-Type: application/json")
    QuestionarioVO salvarQuestionario(NovaPerguntaVO novaPerguntaVO) throws
            AvaliacaoException;

    @RequestLine("DELETE /avaliacoes/{id}")
    Boolean excluirAvaliacao(@Param("id") Long id);

    @RequestLine("DELETE /questionarios/{id}")
    Boolean excluirQuestionario(@Param("id") Integer id) throws
            AvaliacaoException;

    @RequestLine("DELETE /perguntas/{id}")
    Boolean excluirPergunta(@Param("id") Long id) throws AvaliacaoException;

    @RequestLine("GET /usuarios")
    List<UsuarioVO> buscarUsuarios();

    @RequestLine("DELETE /usuarios/{id}")
    Boolean excluirUsuario(@Param("id") Long id);

    @RequestLine("POST /usuarios")
    @Headers("Content-Type: application/json")
    UsuarioVO cadastrarUsuario(NovoUsuarioVO vo);

    @RequestLine("PUT /usuarios")
    @Headers("Content-Type: application/json")
    UsuarioVO atualizarUsuario(NovoUsuarioVO vo);

    @RequestLine("GET /usuarios/{id}")
    UsuarioVO buscarUsuario(@Param("id") Long id);

    @RequestLine("GET /usuarios/login/{login}")
    UsuarioVO buscarUsuario(@Param("login") String login);

    @RequestLine("GET /perguntas/{id}")
    PerguntaVO buscarPergunta(@Param("id") Long id);

    @RequestLine("GET /questionarios/{id}/perguntas")
    List<PerguntaVO> buscarPerguntasDoQuestionario(@Param("id") Integer id);

    @RequestLine("GET /relatorios/questionarios/{questionarioId}/avaliacoes"
            + "/{avaliadoId}/exportacao")
    List<ExportacaoVO> exportar(@Param("questionarioId") Integer
                                        questionarioId, @Param("avaliadoId")
                                        Long avaliadoId);

    @RequestLine("GET /relatorios/questionarios/{questionarioId}/perguntas"
            + "/{perguntaId}/graficos")
    List<GraficoVO> getGraficosInfo(@Param("questionarioId") Integer
                                            questionarioId, @Param
                                            ("perguntaId") Long perguntaId);

    @RequestLine("GET /questionarios/{questionarioId}/controleAcesso")
    ControleAcessoQuestionarioVO getControleAcessoQuestionario(
            @Param("questionarioId") Integer questionarioId);

    @RequestLine("POST /questionarios/controleAcesso")
    @Headers("Content-Type: application/json")
    ControleAcessoQuestionarioVO atualizarControleAcessoQuestionario(
            ControleAcessoQuestionarioVO controleAcessoQuestionarioVO);
}
