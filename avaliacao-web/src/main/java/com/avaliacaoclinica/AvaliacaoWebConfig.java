package com.avaliacaoclinica;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.avaliacaoclinica.client.AvaliacaoApiClient;
import com.avaliacaoclinica.error.AvaliacaoErrorDecoder;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;

/**
 * @author s2it_gmascarin
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 10/19/17 6:54 PM
 */
@Configuration
public class AvaliacaoWebConfig {

    @Value("${avaliacaoApi.endpoint.base-url:}")
    private String avaliacaoApiEndpoint;

    @Bean
    public AvaliacaoApiClient avaliacaoApiClient() {
        final GsonDecoder decoder = new GsonDecoder();

        if (StringUtils.isBlank(avaliacaoApiEndpoint)) {
            return null;
        }

        return Feign.builder()
                .decoder(decoder)
                .encoder(new GsonEncoder())
                .errorDecoder(new AvaliacaoErrorDecoder(decoder))
                .target(AvaliacaoApiClient.class, avaliacaoApiEndpoint);
    }
}
