package com.avaliacaoclinica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
@SpringBootApplication
public class AvaliacaoWebApplication {

    public static void main(final String[] args) {
        SpringApplication.run(AvaliacaoWebApplication.class, args);
    }
}
